import EventEmitter from 'events';

import ButtonMatrix from '../core/ButtonMatrix';

import { Score } from '.';
import { ButtonConfig, PlayerButton } from '../interfaces';
import { ButtonState, ButtonType, PlayerEvent } from '../enums';
import Group from './Group';
import { PlayerInteractions } from '../enums';

export default class Player extends EventEmitter {
    private readonly id: number;
    private readonly name: string;
    private readonly interactions: Array<PlayerInteractions>;

    private score: Score;
    private group: Group;
    private buttonMatrix: ButtonMatrix;
    private buttons: Map<ButtonType, PlayerButton>;
    private canRedirectBall: boolean;
    private eliminated: boolean;

    constructor(
        name: string,
        playerId: number,
        group: Group,
        buttonMatrix: ButtonMatrix,
        playerConfig: ButtonConfig,
    ) {
        super();

        this.buttons = new Map([
            [ButtonType.UP, { id: playerConfig.up, state: ButtonState.UP }],
            [ButtonType.LEFT, { id: playerConfig.left, state: ButtonState.UP }],
            [ButtonType.RIGHT, { id: playerConfig.right, state: ButtonState.UP }],
            [ButtonType.CATCH, { id: playerConfig.catch, state: ButtonState.UP }],
        ]);

        this.id = playerId;
        this.name = name;
        this.buttonMatrix = buttonMatrix;
        this.canRedirectBall = false;
        this.eliminated = false;
        this.score = new Score();
        this.interactions = [];
        this.group = group;
    }

    public animate(): void {
        throw new Error('Method not implemented.');
    }

    private enableBallRedirection(): void {
        this.canRedirectBall = true;
    }

    public disableBallRedirection(): void {
        this.canRedirectBall = false;
    }

    public checkPressedButton(): void {
        this.buttons.forEach((btnInfo, btnDirection) => {
            const btnPressed = this.buttonMatrix.isButtonPressed(btnInfo.id);

            if (btnPressed && btnInfo.state === ButtonState.UP) {
                this.enableBallRedirection();
                this.emit(PlayerEvent.ON_BUTTON_PRESSED, this, btnDirection, btnInfo.state);
                btnInfo.state = ButtonState.DOWN;
            } else if (!btnPressed && btnInfo.state === ButtonState.DOWN) {
                this.disableBallRedirection();
                this.emit(PlayerEvent.ON_BUTTON_RELEASE, this, btnDirection, btnInfo.state);
                btnInfo.state = ButtonState.UP;
            }
        });
    }

    public clearInteractions(interactionToRemove?: PlayerInteractions): void {
        if (this.interactions.length === 0) return;
        if (interactionToRemove === undefined)
            this.interactions.splice(0, this.interactions.length);
        else {
            const interactionIndex = this.interactions.indexOf(interactionToRemove);
            if (interactionIndex === -1) return;
            this.interactions.splice(interactionIndex, 1);
        }
    }

    public cleanScore(): void {
        this.score = new Score();
    }

    public setEliminated(value: boolean): void {
        this.eliminated = value;
    }

    public setGroup(group: Group): void {
        this.group = group;
    }

    public setInteractions(...interactions: Array<PlayerInteractions>) {
        this.interactions.concat(interactions);
    }

    public getScore(): Score {
        return this.score;
    }

    public getId(): number {
        return this.id;
    }

    public getGroup(): Group {
        return this.group;
    }

    public getName(): string {
        return this.name;
    }

    public getInteractions(): Array<PlayerInteractions> {
        return this.interactions;
    }

    public isBallRedirectionEnabled(): boolean {
        return this.canRedirectBall;
    }

    public isEliminated(): boolean {
        return this.eliminated;
    }

    public hasInteractions(interaction?: PlayerInteractions): boolean {
        if (interaction === undefined) return this.interactions.length > 0;
        else return this.interactions.filter((i) => i === interaction).length > 0;
    }

    public toString(): string {
        const id = this.id;
        const name = this.name;
        const group = this.group.getId();
        const score = this.score.getValue();
        const isEliminated = this.eliminated;

        return `Player '${id}' - Name : ${name} | Group ID : ${group} | Eliminated : ${isEliminated} | Score Value : ${score}`;
    }
}
