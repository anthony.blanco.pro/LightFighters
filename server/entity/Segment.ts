import Board from '../core/Board';

import { Ball, Intersection, StaticArea } from '.';
import {
    AreaDrawDirection,
    BallDirection,
    BallInteractions,
    BallStatus,
    SegmentPosition,
} from '../enums';
import { Animable, SegmentNeighbor } from '../interfaces';

export default class Segment implements Animable {
    private readonly id: number;
    private readonly board: Board;
    private readonly pixelIndices: Array<number>;

    private balls: Array<Ball>;
    private areas: Array<StaticArea>;
    private waitingBalls: Array<Ball>;

    private nextSwitching: number;

    private beginIntersection!: Intersection;
    private endIntersection!: Intersection;
    private animationInProgress: boolean;

    constructor(id: number, pixelIndices: number[], board: Board) {
        this.id = id;
        this.pixelIndices = pixelIndices;

        this.areas = [];
        this.balls = [];

        this.board = board;
        this.waitingBalls = [];
        this.nextSwitching = -1;
        this.animationInProgress = false;
    }

    public draw(board: Board) {
        this.areas.forEach((area) => area.draw(board, this.pixelIndices));
        this.balls.forEach((b) => b.draw(board));
        this.manageSwitching();
    }

    private manageSwitching(): void {
        const nextTickBalls: Array<Ball> = [];
        this.balls.forEach((ball: Ball) => {
            const ballStatus = ball.getStatus();
            if (
                ball.isLeavingSegment(this.pixelIndices.length) &&
                ballStatus !== BallStatus.LEAVING
            ) {
                ball.setStatus(BallStatus.LEAVING);
                this.switchBall(ball);
                nextTickBalls.push(ball);
            } else if (
                ball.isOutSegment(this.pixelIndices.length) &&
                ballStatus === BallStatus.LEAVING
            ) {
                ball.stopMove();
            } else {
                nextTickBalls.push(ball);
            }
        });

        this.balls = nextTickBalls.concat(this.waitingBalls);
        this.waitingBalls = [];
    }

    private switchBall(ball: Ball): void {
        let nextNeighbor: SegmentNeighbor;
        const ballNextNeighbor = ball.getNextNeighbor();
        if (ballNextNeighbor !== null) {
            nextNeighbor = ballNextNeighbor;
        } else {
            this.setNextSwitching(ball.getDirection());

            // Get intersection neighbor except himself
            const nextNeighborArray =
                ball.getDirection() === BallDirection.FORWARD
                    ? this.endIntersection.getNeighbors(this.id)
                    : this.beginIntersection.getNeighbors(this.id);

            // if no other neighbors, set next neighbor as himself
            if (nextNeighborArray.length === 0) {
                nextNeighbor =
                    ball.getDirection() === BallDirection.FORWARD
                        ? this.endIntersection.getNeighbors()[0]
                        : this.beginIntersection.getNeighbors()[0];
            } else {
                nextNeighbor = nextNeighborArray[this.nextSwitching];
            }
        }

        if (nextNeighbor === undefined) return;

        nextNeighbor.reference.createBall(
            ball.getId(),
            ball.getModel(),
            ball.getSpeed(),
            nextNeighbor.direction === SegmentPosition.BEGIN
                ? BallDirection.FORWARD
                : BallDirection.BACKWARD,
            undefined,
            undefined,
            undefined,
            ball.getInteractions(),
            ball.getAnimations(),
        );
    }

    public createBall(
        id: number,
        model: Uint32Array,
        speed: number,
        direction: BallDirection,
        positionOffset = 0,
        startMove = true,
        redirectable = true,
        interactions: Array<BallInteractions> = [],
        animations: Array<[number, number]> = [],
    ): Ball {
        const newBall = new Ball(
            id,
            model,
            speed,
            direction === BallDirection.FORWARD
                ? positionOffset
                : this.pixelIndices.length - (1 + positionOffset), // Segment Pos
            direction,
            this,
            redirectable,
        );

        newBall.setInteractions(interactions);
        newBall.setAnimations(animations);

        if (startMove) newBall.startMove();
        this.setNextSwitching(newBall.getDirection());
        this.waitingBalls.push(newBall);
        return newBall;
    }

    public createWall(id: number, position: SegmentPosition): void {
        this.setIntersection(
            position,
            new Intersection(id, [{ reference: this, direction: position }], this.board),
        );
    }

    public reset(): void {
        this.balls = [];
        this.areas = [];
        this.waitingBalls = [];
        this.removeBalls();
    }

    public removeBalls(...ballIds: Array<number>) {
        const ballKept: Array<Ball> = [];
        this.balls.forEach((ball) => {
            if (!ballIds.includes(ball.getId())) {
                ballKept.push(ball);
            }
        });
        this.balls = ballKept;
    }

    public animate(models: Uint32Array, timing: number[]): void {
        this.animationInProgress = true;
        if (models.length != timing.length || models.length === 0) return;

        const areaId = this.addStaticArea(new Uint32Array(this.pixelIndices).fill(models[0]));
        this.draw(this.board);
        this.board.flush();
        setTimeout(() => {
            this.removeStaticArea(areaId);
            this.draw(this.board);
            this.board.flush();
            return this.animate(models.slice(1), timing.slice(1));
        }, timing[0]);

        this.animationInProgress = false;
    }

    public addStaticArea(
        model: Uint32Array,
        segmentPosition = 0,
        direction = AreaDrawDirection.FORWARD,
    ): number {
        return this.areas.push(new StaticArea(model, segmentPosition, direction)) - 1;
    }

    public removeStaticArea(id?: number): void {
        this.areas.splice(id ?? 0, id === undefined ? undefined : 1);
    }

    private setNextSwitching(direction: BallDirection): void {
        // Check number of neighbor (except himself) : if count === 0, only himself is available
        const neighborCount =
            direction === BallDirection.FORWARD
                ? this.endIntersection.getNeighborSegments(this.id).length
                : this.beginIntersection.getNeighborSegments(this.id).length;
        this.nextSwitching = Math.floor(Math.random() * neighborCount);
    }

    public setIntersection(position: SegmentPosition, intersection: Intersection): Segment {
        if (position === SegmentPosition.BEGIN) {
            this.beginIntersection = intersection;
        } else {
            this.endIntersection = intersection;
        }
        return this;
    }

    public setStaticAreas(areas: Array<StaticArea>): Segment {
        this.areas = areas;
        return this;
    }

    public getId(): number {
        return this.id;
    }

    public getBalls(...ids: number[]): Array<Ball> {
        if (ids.length === 0) return this.balls;
        return this.balls.filter((b) => ids.includes(b.getId()));
    }

    public getIntersection(position: SegmentPosition): Intersection {
        if (position === SegmentPosition.BEGIN) {
            return this.beginIntersection;
        } else {
            return this.endIntersection;
        }
    }

    public getStaticArea(...ids: number[]): Array<StaticArea> {
        if (ids.length === 0) return this.areas;
        return this.areas.filter((a, idx) => ids.includes(idx));
    }

    public getPixels(): Array<number> {
        return this.pixelIndices;
    }

    public isAnimationsInProgress(): boolean {
        return this.animationInProgress;
    }

    public toString(): string {
        const id = this.id;
        const pixelsLength = this.pixelIndices.length;
        const beginIntersectionId = this.beginIntersection.getId();
        const endIntersectionId = this.endIntersection.getId();

        return `Segment '${id}' - Pixels Len : ${pixelsLength} | Begin Int ID : ${beginIntersectionId} | End Int ID : ${endIntersectionId}`;
    }
}
