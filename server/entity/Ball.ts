import Board from '../core/Board';

import { Segment, Area } from '.';
import { SegmentNeighbor } from '../interfaces';
import { AreaDrawDirection, BallDirection, BallInteractions, BallStatus } from '../enums';
import Group from './Group';

export default class Ball extends Area {
    private readonly id: number;
    private readonly direction: BallDirection;
    private readonly currSegment: Segment;
    private readonly ballRedirection: boolean;

    private ownerGroup!: Group;
    private speed: number; // pixels per seconds
    private intervalId!: NodeJS.Timer;
    private segmentPosition: number;
    private status: BallStatus;
    private nextNeighbor: SegmentNeighbor | null;
    private isPaused!: boolean;

    private interactions: Array<BallInteractions>;

    constructor(
        id: number,
        model: Uint32Array = new Uint32Array(0),
        speed = 1,
        segmentPosition = 0,
        direction = BallDirection.FORWARD,
        segment: Segment,
        ballRedirection = true,
    ) {
        super(model);

        this.id = id;
        this.speed = speed;
        this.segmentPosition = segmentPosition;
        this.direction = direction;
        this.status = BallStatus.IDLE;
        this.ballRedirection = ballRedirection;
        this.nextNeighbor = null;
        this.currSegment = segment;
        this.isPaused = false;

        this.interactions = [];
    }

    public draw(board: Board): void {
        super.draw(
            board,
            this.currSegment.getPixels(),
            this.segmentPosition,
            this.direction === BallDirection.FORWARD
                ? AreaDrawDirection.BACKWARD
                : AreaDrawDirection.FORWARD,
        );
    }

    /**
     * Start the movement of the ball depends on ball direction, (Start an interval with frequency 1/speed)
     */
    public startMove(): void {
        this.intervalId = setInterval(() => {
            if (this.isPaused) return;
            this.segmentPosition += 1 * this.direction;
        }, 1000 / this.speed);
    }

    public stopMove(): void {
        clearInterval(this.intervalId);
    }

    public addInteraction(interaction: BallInteractions) {
        this.interactions.push(interaction);
    }

    public clearInteractions(interactionToRemove?: BallInteractions): void {
        if (this.interactions.length === 0) return;
        if (interactionToRemove === undefined)
            this.interactions.splice(0, this.interactions.length);
        else {
            const interactionIndex = this.interactions.indexOf(interactionToRemove);
            if (interactionIndex === -1) return;
            this.interactions.splice(interactionIndex, 1);
        }
    }

    public pause() {
        this.isPaused = true;
    }

    public resume() {
        this.isPaused = false;
    }

    public setGroup(group: Group): void {
        this.ownerGroup = group;
    }

    public setStatus(status: BallStatus): void {
        this.status = status;
    }

    public setSpeed(speed: number): void {
        this.speed = speed;
    }

    public setNextNeighbor(neighbor: SegmentNeighbor | null): void {
        this.nextNeighbor = this.ballRedirection ? neighbor : null;
    }

    public setInteractions(interactions: Array<BallInteractions>) {
        this.interactions = interactions;
    }

    public getId(): number {
        return this.id;
    }

    public getGroup(): Group | undefined {
        return this.ownerGroup;
    }

    public getDirection(): BallDirection {
        return this.direction;
    }

    public getStatus(): BallStatus {
        return this.status;
    }

    public getCurrentSegment(): Segment {
        return this.currSegment;
    }

    public getSegmentPosition(): number {
        return this.segmentPosition;
    }

    public getSpeed(): number {
        return this.speed;
    }

    public getNextNeighbor(): SegmentNeighbor | null {
        return this.nextNeighbor;
    }

    public getInteractions(): Array<BallInteractions> {
        return this.interactions;
    }

    public isLeavingSegment(availablePixelsLength: number): boolean {
        return this.segmentPosition < 0 || this.segmentPosition >= availablePixelsLength;
    }

    public isOutSegment(availablePixelsLength: number): boolean {
        return (
            this.segmentPosition + this.model.length - 1 < 0 ||
            this.segmentPosition - this.model.length + 1 >= availablePixelsLength
        );
    }

    public isAlreadyRedirected(): boolean {
        return this.nextNeighbor !== null;
    }

    public hasInteractions(interaction?: BallInteractions): boolean {
        if (interaction === undefined) return this.interactions.length > 0;
        else return this.interactions.filter((i) => i === interaction).length > 0;
    }

    public toString(): string {
        const id = this.id;
        const ownerGroup = this.ownerGroup;
        const status = this.status;
        const modelLength = this.model.length;
        const currSegId = this.currSegment.getId();

        return `Ball '${id}' Status : ${status} | Model len: ${modelLength} | Group ID : ${ownerGroup} | Current Segment ID : ${currSegId}`;
    }
}
