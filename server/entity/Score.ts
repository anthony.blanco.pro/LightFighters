export default class Score {
    private maxScore: number;
    private lastValue: number;
    private value: number;

    constructor() {
        this.value = 0;
        this.lastValue = -1;
        this.maxScore = -1;
    }

    public computeValue(value: number): void {
        this.lastValue = this.value;
        this.value += value;
    }

    public reset() {
        this.value = 0;
        this.lastValue = -1;
        this.maxScore = -1;
    }

    public setValue(newScore: number): void {
        this.lastValue = this.value;
        this.value = newScore;
    }

    public setMaxValue(maxScore: number): void {
        this.maxScore = maxScore;
    }

    public getValue(): number {
        return this.value;
    }

    public getMaxValue(): number {
        return this.maxScore;
    }

    public isChanged(): boolean {
        return this.value !== this.lastValue;
    }

    public isScoreReachMaxValue(): boolean {
        return this.maxScore != -1 || this.value >= this.maxScore;
    }
}
