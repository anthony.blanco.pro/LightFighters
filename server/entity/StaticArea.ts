import Board from '../core/Board';

import { Area } from '.';
import { AreaDrawDirection } from '../enums';

export default class StaticArea extends Area {
    private readonly direction: AreaDrawDirection;
    private readonly segmentPosition: number;

    constructor(model: Uint32Array, segmentPosition = 0, direction = AreaDrawDirection.FORWARD) {
        super(model);
        this.segmentPosition = segmentPosition;
        this.direction = direction;
    }

    public draw(board: Board, segmentPixels: Array<number>): void {
        super.draw(board, segmentPixels, this.segmentPosition, this.direction);
    }

    public getSegmentPosition(): number {
        return this.segmentPosition;
    }

    public getDirection(): AreaDrawDirection {
        return this.direction;
    }
}
