import Board from '../core/Board';
import { AreaDrawDirection } from '../enums';
import { Animable } from '../interfaces';
import { sleep, validate } from '../utils/utils';

export default abstract class Area implements Animable {
    protected model: Uint32Array;

    protected pixels: Map<number, number>;
    private readonly nextPixels: Map<number, number>;

    private animations: Array<[number, number]>;
    private animationModel?: Uint32Array;
    private animationsInProgress: boolean;

    protected constructor(model: Uint32Array) {
        this.model = model;
        this.pixels = new Map();
        this.nextPixels = new Map();
        this.animations = [];
        this.animationsInProgress = false;
    }

    public draw(
        board: Board,
        segmentPixels: Array<number>,
        segmentPosition: number,
        direction: AreaDrawDirection,
    ): void {
        this.nextPixels.clear();
        const nextModel = this.manageAnimations();
        nextModel.forEach((pixelColor, idx) => {
            const offset = segmentPosition + idx * direction;
            if (offset < 0 || offset >= segmentPixels.length) return;
            this.nextPixels.set(segmentPixels[offset], pixelColor);
        });
        board.setPixelMap(this.nextPixels);
    }

    private manageAnimations(): Uint32Array {
        if (this.animationsInProgress && this.animationModel) return this.animationModel;
        if (this.animations.length === 0) return this.model;
        this.animationsInProgress = true;
        const [nextAnimationModel, nextAnimationTiming] = validate(this.animations.shift());

        this.animationModel = new Uint32Array(this.model.length).fill(nextAnimationModel);
        sleep(nextAnimationTiming).then(() => {
            this.animationsInProgress = false;
            this.animationModel = undefined;
        });
        return this.animationModel;
    }

    public animate(models: Uint32Array, timing: Array<number>) {
        if (models.length !== timing.length)
            throw new Error('Animation models lenght must be equals to timing array lenght');

        models.forEach((model, idx) => {
            this.animations.push([model, timing[idx]]);
        });
    }

    protected setPixels(pixels: Map<number, number>): void {
        this.pixels = pixels;
    }

    public setModel(model: Uint32Array): void {
        this.model = model;
    }

    public setAnimations(animations: Array<[number, number]>) {
        this.animations = animations;
        this.animationModel = undefined;
    }

    protected getPixels(): Map<number, number> {
        return this.pixels;
    }

    public getModel(): Uint32Array {
        return this.model;
    }

    public getAnimations(): Array<[number, number]> {
        return this.animations;
    }

    public isAnimationsInProgress(): boolean {
        return this.animationsInProgress;
    }
}
