import Board from '../core/Board';

import { Ball, Player, Segment, StaticArea } from '.';
import { Animable, SegmentNeighbor } from '../interfaces';
import {
    AreaDrawDirection,
    BallDirection,
    ButtonType,
    SegmentPosition,
    IntersectionAreaType,
    IntersectionEvents,
    BallInteractions,
} from '../enums';

import EventEmitter from 'events';
import { validate } from '../utils/utils';
import Group from './Group';

export default class Intersection extends EventEmitter implements Animable {
    private readonly id: number;
    private readonly drawAreas!: Map<SegmentNeighbor, StaticArea>;

    private board: Board;
    private groups: Array<Group>;
    private neighbors!: Map<SegmentNeighbor, ButtonType>;
    private animationsInProgress: boolean;

    constructor(id: number, segments: Array<SegmentNeighbor>, board: Board) {
        super();

        this.id = id;
        this.groups = [];
        this.board = board;
        this.drawAreas = new Map();
        this.neighbors = segments.reduce((acc, neighbor): Map<SegmentNeighbor, ButtonType> => {
            acc.set(neighbor, neighbor.buttonType ?? ButtonType.UNKNOWN);
            return acc;
        }, new Map());
        this.animationsInProgress = false;
    }

    public animate(models: Uint32Array, timing: Array<number>): void {
        this.animationsInProgress = true;

        if (!this.isDrawable()) this.setModel(new Uint32Array([0xffffff]));

        this.drawAreas.forEach((neighborArea) => {
            neighborArea.animate(models, timing);
        });

        if (!this.isDrawable()) this.drawAreas.clear();
        this.animationsInProgress = false;
    }

    public reset() {
        this.groups = [];
        this.drawAreas.clear();
        this.animationsInProgress = false;
        this.removeAllListeners();
    }

    public draw(board: Board): void {
        if (this.isDrawable()) {
            this.drawAreas.forEach((neighborArea, { reference }) => {
                neighborArea.draw(board, reference.getPixels());
            });
        }
        this.manageEvents();
    }

    private manageEvents() {
        this.drawAreas.forEach((area, neighbor) => {
            neighbor.reference.getBalls().forEach((ball: Ball) => {
                const ballSegPos = ball.getSegmentPosition();
                const areaSegPos = area.getSegmentPosition();
                const areaBeginSegPos =
                    neighbor.direction === SegmentPosition.BEGIN
                        ? areaSegPos
                        : areaSegPos - area.getModel().length;
                const areaEndSegPos =
                    neighbor.direction === SegmentPosition.BEGIN
                        ? area.getModel().length
                        : areaSegPos;

                const ballIsInArea = ballSegPos >= areaBeginSegPos && ballSegPos <= areaEndSegPos;

                if (!ball.isLeavingSegment(neighbor.reference.getPixels().length)) {
                    if (ballIsInArea && !ball.hasInteractions(BallInteractions.ENTERING_AREA))
                        this.emit(IntersectionEvents.ON_BALL_ENTERING_IN_AREA, ball);

                    if (
                        !ballIsInArea &&
                        !ball.hasInteractions(BallInteractions.LEAVING_AREA) &&
                        ball.hasInteractions(BallInteractions.ENTERING_AREA)
                    ) {
                        this.emit(IntersectionEvents.ON_BALL_LEAVING_AREA, ball);
                        ball.clearInteractions();
                    }
                }
            });
        });
    }

    public addGroup(group: Group): Intersection {
        if (this.groups.find((g) => g.getId() === group.getId()) !== undefined) return this;
        this.groups.push(group);
        return this;
    }

    public removeGroup(groupId: number) {
        this.groups = this.groups.filter((g) => g.getId() !== groupId);
    }

    public setSegments(segments: Map<SegmentNeighbor, ButtonType>): Intersection {
        this.neighbors = segments;
        return this;
    }

    public setModel(model: Uint32Array): Intersection {
        this.neighbors.forEach((_, neighbor) => {
            const drawDirection =
                neighbor.direction === SegmentPosition.BEGIN
                    ? AreaDrawDirection.FORWARD
                    : AreaDrawDirection.BACKWARD;

            this.drawAreas.set(
                neighbor,
                new StaticArea(
                    model,
                    drawDirection === AreaDrawDirection.FORWARD
                        ? 0
                        : neighbor.reference.getPixels().length - 1, // Segment Pos
                    drawDirection,
                ),
            );
        });

        return this;
    }

    public getId(): number {
        return this.id;
    }

    public getGroups(): Array<Group> {
        return this.groups;
    }

    public getGroupsId(): Array<number> {
        return this.groups.map((g) => g.getId());
    }

    public getNeighborByButtonType(btnType: ButtonType): SegmentNeighbor {
        const segmentNeighbor = Array.from(this.neighbors.entries())
            .filter((neighbor) => neighbor[1] === btnType)
            .map((s) => s[0]);
        return validate(segmentNeighbor[0], new Error(`No segment found for button ${btnType}`));
    }

    public getNeighborSegments(...unexceptedIds: number[]): Array<Segment> {
        return this.getNeighbors(...unexceptedIds).map((neighbor) => neighbor.reference);
    }

    public getNeighbors(...unexceptedIds: number[]): Array<SegmentNeighbor> {
        return Array.from(this.neighbors.keys()).filter(
            (neighbor) => !unexceptedIds.includes(neighbor.reference.getId()),
        );
    }

    public getBalls(...player: Array<Player>): Map<Ball, IntersectionAreaType> {
        return Array.from(this.drawAreas).reduce((acc, [neighbor, area]) => {
            const segBalls = neighbor.reference.getBalls();

            segBalls.forEach((ball) => {
                if (this.isBallInArea(ball, area)) {
                    if (
                        ((ball.getDirection() === BallDirection.FORWARD &&
                            area.getDirection() === AreaDrawDirection.FORWARD) ||
                            (ball.getDirection() === BallDirection.BACKWARD &&
                                area.getDirection() === AreaDrawDirection.BACKWARD)) &&
                        (player === undefined ||
                            ball.getGroup() === undefined ||
                            player.find(
                                (p) =>
                                    (p.getGroup()?.getId() === ball.getGroup()?.getId()) !==
                                    undefined,
                            ))
                    ) {
                        acc.set(ball, IntersectionAreaType.PASSIVE_AREA);
                    } else {
                        acc.set(ball, IntersectionAreaType.ACTIVE_AREA);
                    }
                }
            });
            return acc;
        }, new Map<Ball, IntersectionAreaType>());
    }

    public isDrawable(): boolean {
        return this.drawAreas !== undefined && this.drawAreas.size > 0;
    }

    private isBallInArea(ball: Ball, area: StaticArea) {
        return (
            (area.getDirection() === AreaDrawDirection.FORWARD &&
                ball.getSegmentPosition() < area.getSegmentPosition() + area.getModel().length) ||
            (area.getDirection() === AreaDrawDirection.BACKWARD &&
                ball.getSegmentPosition() > area.getSegmentPosition() - area.getModel().length)
        );
    }

    public hasSegmentsNeighbor(...segments: Array<Segment>) {
        segments.forEach((segment) => {
            if (
                this.getNeighbors().filter((neighbor) => neighbor.reference === segment).length ===
                0
            )
                return false;
        });
        return true;
    }

    public isAnimationsInProgress(): boolean {
        let neighborsAreAnimating = false;
        Array.from(this.drawAreas.values()).forEach((area) => {
            neighborsAreAnimating = neighborsAreAnimating || area.isAnimationsInProgress();
        });

        return this.animationsInProgress && neighborsAreAnimating;
    }

    public toString(): string {
        const id = this.id;
        const groupsId = this.groups.map((g) => g.getId()).join(', ');
        const neighborsId = Array.from(this.neighbors.keys())
            .map((n) => n.reference.getId())
            .join(', ');

        return `Intersection '${id}' - Groups : [${groupsId}] | Neighbors : [${neighborsId}]`;
    }
}
