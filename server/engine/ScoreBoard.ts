import { SerialPortStream } from '@serialport/stream';

import { Player } from '../entity';
import { DisplayLayout } from '../enums';
import { Runnable } from '../interfaces';

/**
 * Command to send on serial port :
 * - SCORE         -> !0aaaaaabbbbbbccccccdddddd"
 * - LIFECOUNT     -> !1aa****bb****cc****dd****"
 * - LOADING       -> !2************************"
 * - SPLASH        -> !3************************"
 * - SLEEP         -> !4************************"
 * - TIMELEFT      -> !5xxxxxxxxxxxxxxxxxxxxxxxx"
 * - RANKS         -> !6aaaaaabbbbbbccccccdddddd"
 * - PERSONAL_RANK -> !7a*****b*****c*****d*****"
 * - CREDIT        -> !8************************"
 */

const INDIVIDUAL_DATA_LENGTH = 6;
const SERIAL_DATA_LENGTH = 27;

const ROTATION_LOOP_DELAY = 500;
const UPDATE_LOOP_DELAY = 250;

export default class ScoreBoard extends Runnable {
    private serial: SerialPortStream;
    public players: Array<Player>;
    private currDisplay: DisplayLayout;
    private displayRotation: Map<number, [DisplayLayout, number]>; // <priority, [layout, duration]>

    public gameTime: number;
    private keepCurrentDisplay: boolean;
    private displaying: boolean;

    constructor(serialPort: SerialPortStream) {
        super(UPDATE_LOOP_DELAY, true);
        this.players = [];
        this.serial = serialPort;
        this.currDisplay = DisplayLayout.SPLASH;
        this.displayRotation = new Map();
        this.gameTime = 0;
        this.keepCurrentDisplay = false;
        this.displaying = false;
    }

    public startRotation(): void {
        this.newLoop(() => this.rotationLoop(), ROTATION_LOOP_DELAY);
    }

    protected loop(): void {
        let data = '!' + this.currDisplay;

        switch (this.currDisplay) {
            case DisplayLayout.SCORE:
                this.players.forEach((p) => {
                    const score = p.getScore().getValue().toString();
                    data += score.padEnd(INDIVIDUAL_DATA_LENGTH, '*');
                });
                break;
            case DisplayLayout.LIFE_COUNT:
                this.players.forEach((p) => {
                    const liftCount = p.getScore().getValue();
                    const maxLiftCount = p.getScore().getMaxValue();
                    data += `${maxLiftCount}${liftCount}`.padEnd(INDIVIDUAL_DATA_LENGTH, '*');
                });
                break;
            case DisplayLayout.TIME_LEFT:
                for (let idx = 0; idx < this.players.length; idx++)
                    data += Math.floor(this.gameTime)
                        .toString()
                        .padEnd(INDIVIDUAL_DATA_LENGTH, '*');
                break;
            case DisplayLayout.RANKS:
                this.getPlayersOrderByRank().forEach((player) => {
                    data += `${player.getName()}`.padEnd(INDIVIDUAL_DATA_LENGTH, '*');
                });
                break;
            case DisplayLayout.PERSONAL_RANK:
                this.getPlayerOrderById().forEach((player) => {
                    const rank = this.getPlayerRank(player).toString();
                    data += rank.padEnd(INDIVIDUAL_DATA_LENGTH, '*');
                });
                break;
            default:
                break;
        }

        data = data.padEnd(SERIAL_DATA_LENGTH - 1, '*');
        data += '"';

        this.serial.write(data);
    }

    private rotationLoop(): void {
        if (this.keepCurrentDisplay) return;
        if (this.displaying) return;
        this.displaying = true;

        const nextDisplay = this.getNextDisplay();
        this.currDisplay = nextDisplay.display;

        setTimeout(() => {
            this.displaying = false;
        }, nextDisplay.duration);
    }

    public addPlayer(player: Player): void {
        this.players.push(player);
    }

    public addDisplay(priority: number, display: DisplayLayout, duration: number): void {
        this.displayRotation.set(priority, [display, duration]);
    }

    public removeDisplay(display: DisplayLayout): void {
        if (this.displayRotation.size > 0) return;

        this.displayRotation = new Map(
            Array.from(this.displayRotation.entries()).filter((d) => d[1][0] !== display),
        );
    }

    public unlockDisplay(): void {
        this.keepCurrentDisplay = false;
    }

    public clearDisplay(): void {
        this.displayRotation.clear();
    }

    public reset(): void {
        this.players = [];
        this.gameTime = 0;

        this.unlockDisplay();
        this.clearDisplay();
        this.keepCurrentDisplay = false;

        this.stop();
        this.stopExtraLoop();
    }

    public resetPlayersScore(): void {
        this.players.forEach((player) => {
            player.getScore().reset();
        });
    }

    private getNextDisplay(): { display: DisplayLayout; duration: number } {
        const displays = Array.from(this.displayRotation.values());
        const currDisplayIdx = displays
            .map((displayArray) => displayArray[0])
            .findIndex((d) => d === this.currDisplay);
        const nextRotation = displays[currDisplayIdx + 1] ??
            displays[0] ?? [DisplayLayout.SPLASH, -1];

        return { display: nextRotation[0], duration: nextRotation[1] };
    }

    public getPlayerRank(player: Player): number {
        let rank = 1;
        this.players.forEach((currPlayer) => {
            if (
                currPlayer !== player &&
                currPlayer.getScore().getValue() > player.getScore().getValue()
            )
                rank += 1;
        });
        return rank;
    }

    public getPlayerOrderById(): Map<number, Player> {
        const playerOrderById = new Map<number, Player>();
        this.players.forEach((p) => playerOrderById.set(p.getId(), p));
        return new Map([...playerOrderById.entries()].sort());
    }

    public getPlayersOrderByRank(): Map<number, Player> {
        const playerOrderByRank = new Map<number, Player>();
        this.players.forEach((p) => playerOrderByRank.set(this.getPlayerRank(p), p));
        return new Map([...playerOrderByRank.entries()].sort());
    }

    public setDisplay(display: DisplayLayout, lockDisplay = false) {
        this.keepCurrentDisplay = lockDisplay;
        this.currDisplay = display;
        this.loop();
    }
}
