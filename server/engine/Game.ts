import EventEmitter from 'events';

import Board from '../core/Board';

import { ScoreBoard } from '../engine';
import { Ball, Group, Intersection, Player, Segment } from '../entity';

import {
    Animable,
    Cores,
    DisplayRotationProperties,
    GameCores,
    GameMetaData,
    GameMode,
    GameModeBall,
    GameModeGroup,
    GameModePlayer,
    GameRequest,
    Runnable,
} from '../interfaces';

import {
    BallDirection,
    BallInteractions,
    ButtonType,
    DisplayLayout,
    GameDifficulty,
    GameEvents,
    GameProperty,
    GameStatus,
    GameStoppedReason,
    GameType,
    IntersectionAreaType,
    IntersectionEvents,
    PlayerEvent,
    PlayerInteractions,
    RunnableStatus,
} from '../enums';

import { validate } from '../utils/utils';
import { Logger } from '../service/Logger';
import { clearTimeout } from 'timers';

const LOGGER = Logger.getInstance();

const GAME_REFRESH_RATE = 1000 / 100;

const PLAYER_DEFAULT_NAME = ['Alpha', 'Beta', 'Gamma', 'Delta'];
const PLAYER_DEFAULT_GROUP = [1, 2, 3, 4];
const PLAYER_DEFAULT_INTERSECTION = [3, 7, 5, 1];
const PLAYER_DEFAULT_CONTROLLER = [2, 4, 3, 1];
const PLAYER_DEFAULT_MODEL_SIZE = 4;
const PLAYER_DEFAULT_MODEL_PATTERN = [0xff0000, 0x00ff00, 0x0000ff, 0xffff00];

export default class Game extends Runnable {
    private readonly mode: GameMode;
    private readonly gameCores: GameCores;
    private readonly event: EventEmitter | undefined;

    private gameRequest: GameRequest | undefined;
    private gameProperties: Map<GameProperty, { value: unknown; arg: unknown }>;

    private groups: Array<Group>;
    private players: Array<Player>;
    private ballsId: Array<number>;
    private difficulty: GameDifficulty;
    private store = new Map<unknown, unknown>();

    constructor(gameMode: GameMode, cores: Cores, event: EventEmitter) {
        super(GAME_REFRESH_RATE, false);

        this.mode = gameMode;
        this.difficulty = gameMode.config.defaultDifficulty;

        this.groups = [];
        this.players = [];
        this.ballsId = [];

        this.event = event;
        this.gameProperties = new Map();
        this.gameCores = { ...cores, scoreboard: new ScoreBoard(cores.serialPort) };
    }

    public async init(gameRequest: GameRequest): Promise<void> {
        if (this.status === RunnableStatus.READY) return;

        LOGGER.info(`Initializing the game`);

        if (this.gameRequest === undefined) this.gameRequest = gameRequest;

        this.status = RunnableStatus.LOADING;
        this.difficulty = gameRequest.difficulty as GameDifficulty;

        const players: Array<GameModePlayer> = [];
        for (let idx = 0; idx < gameRequest.playerCount; idx++) {
            players.push({
                id: idx,
                name: `${PLAYER_DEFAULT_NAME[idx]}`,
                controllerId: PLAYER_DEFAULT_CONTROLLER[idx],
                groupId: PLAYER_DEFAULT_GROUP[idx],
                intersectionId: PLAYER_DEFAULT_INTERSECTION[idx],
                model: new Uint32Array(PLAYER_DEFAULT_MODEL_SIZE).fill(
                    PLAYER_DEFAULT_MODEL_PATTERN[idx],
                ),
            });
        }
        await this.createPlayers(players);

        // Launching custom mode program
        this.mode.main(this);
        this.getEventEmitter().emit(GameEvents.BEFORE_INIT);
        this.getEventEmitter().emit(GameEvents.ON_INIT);

        // Synchronize scoreboard time <-> game time
        this.newLoop(() => (this.getScoreBoard().gameTime = this.getGameTimeLeftMs() / 1000), 50);

        // Handle player Event
        LOGGER.debug(`\t- Handling players events`);
        this.players.forEach((p) => {
            p.on(PlayerEvent.ON_BUTTON_PRESSED, (player: Player, btnState: ButtonType) =>
                this.handlePlayerButtonPressed(player, btnState),
            );
            p.on(PlayerEvent.ON_BUTTON_RELEASE, (player: Player, btnState: ButtonType) =>
                this.handlePlayerButtonRelease(player, btnState),
            );
        });

        // Handle Intersection Event
        LOGGER.debug(`\t- Handling intersections events`);
        this.getIntersections().forEach((i) => {
            i.on(IntersectionEvents.ON_BALL_LEAVING_AREA, (ball: Ball) =>
                this.handleIntersectionBallLeaving(ball, i),
            );
            i.on(IntersectionEvents.ON_BALL_ENTERING_IN_AREA, (ball: Ball) =>
                this.handleIntersectionBallEntering(ball, i),
            );
        });

        // Set display properties
        const displayRotationProperty = this.gameProperties.get(GameProperty.S_DISPLAY_ROTATION)
            ?.value as Array<DisplayRotationProperties>;
        displayRotationProperty.forEach((dProperty) => {
            this.getScoreBoard().addDisplay(
                dProperty.priority,
                dProperty.display,
                dProperty.duration,
            );
        });

        // Emit game status
        this.status = RunnableStatus.READY;
        this.getEventEmitter().emit(GameEvents.ON_READY);

        // Resume game configuration
        this.resumeGameProperties();

        // Display waiting screen
        this.getScoreBoard().setDisplay(DisplayLayout.SPLASH, true);
    }

    public start(): void {
        if (this.status !== RunnableStatus.READY)
            throw new Error('Game is not ready to be started');

        LOGGER.info('Starting the game');
        this.getEventEmitter().emit(GameEvents.ON_START);
        this.getScoreBoard().start();
        this.getScoreBoard().startRotation();
        this.getScoreBoard().unlockDisplay();
        super.start();
    }

    public async stop(reason?: GameStoppedReason): Promise<void> {
        LOGGER.info('Stopping the game');
        this.getEventEmitter().emit(GameEvents.ON_STOP, this.gameCores, reason);
        super.stop();
        super.stopExtraLoop();
    }

    public pause(): void {
        LOGGER.info('Pausing the game');
        this.getEventEmitter().emit(GameEvents.ON_PAUSE, this.gameCores);
        this.getScoreBoard().setDisplay(DisplayLayout.PAUSE, false);
        this.getScoreBoard().pause();
        this.getSegments().forEach((s) => s.getBalls().forEach((b) => b.pause()));
        super.pause();
    }

    public resume(): void {
        LOGGER.info('Resuming the game');
        this.getEventEmitter().emit(GameEvents.ON_RESUME, this.gameCores);
        this.getScoreBoard().resume();
        this.getSegments().forEach((s) => s.getBalls().forEach((b) => b.resume()));
        super.resume();
    }

    public reset(): void {
        LOGGER.info('Resetting the game');

        this.store.clear();
        this.gameProperties.clear();

        this.getScoreBoard().reset();
        this.getSegments().forEach((s) => s.reset());
        this.getIntersections().forEach((i) => {
            i.removeAllListeners();
            i.reset();
        });
        this.getPlayers().forEach((p) => p.removeAllListeners());
        this.getBoard().clear();
        this.getBoard().flush();

        this.players = [];
        this.groups = [];
        this.ballsId = [];
        this.status = RunnableStatus.NOT_READY;
    }

    public async clean(): Promise<void> {
        this.reset();
        this.getScoreBoard().setDisplay(DisplayLayout.SPLASH);
        delete this.gameCores.scoreboard;
    }

    public async sleep(): Promise<void> {
        if (this.isStarted()) await this.stop(GameStoppedReason.API_SLEEP_CALL);
        this.reset();

        const ballConfig = [];
        for (let idx = 0; idx < 50; idx++) {
            const model = new Uint32Array(3).fill(Math.floor(Math.random() * 0xffffff));
            const speed = Math.floor(Math.random() * (25 - 5) + 5);
            ballConfig.push({
                id: idx,
                model: model,
                initialSpeed: speed,
                direction: BallDirection.FORWARD,
                segmentId: 10,
            });
        }

        this.createBalls(ballConfig);
        this.newLoop(() => {
            this.getBoard().clear();
            this.getSegments().forEach((s) => s.draw(this.getBoard()));
            this.getBoard().flush();
        }, GAME_REFRESH_RATE);

        this.status = RunnableStatus.SLEEP;
    }

    protected loop(): void {
        // Check game statuses
        if ([RunnableStatus.PAUSED].includes(this.status)) return;

        // Game tick
        this.emit(GameEvents.ON_TICK, this.gameCores);

        // Clear board pixels
        this.getBoard().clear();

        // Read button status (pressed or not) + emit players events if his button is pressed
        this.gameCores.buttonMatrix.read();
        this.players.forEach((p) => p.checkPressedButton());

        // Draw all entities
        this.getIntersections().forEach((i) => i.draw(this.getBoard()));
        this.getSegments().forEach((s) => s.draw(this.getBoard()));

        // Check end of game
        this.manageEndOfGame();

        // Flush pixels
        this.getBoard().flush();

        if (this.status === GameStatus.STOPPED) {
            while (this.isAnimationsInProgress()) this.getBoard().flush();
            this.stop();
        }
    }

    public createGroups(groupsConfig: Array<GameModeGroup>): void {
        LOGGER.info(`\t- Creating group(s)`);
        groupsConfig.forEach((gConfig) => {
            this.groups.push(new Group(gConfig.id, gConfig.name ?? `Equipe #${gConfig.id}`));
        });
    }

    public async createPlayers(playersConfig: Array<GameModePlayer>): Promise<void> {
        return new Promise((resolve) => {
            LOGGER.info(`\t- Creating '${playersConfig.length}' player(s)`);

            const { buttonMatrix, controllers } = this.gameCores;

            // Create 4 groups if no custom group given
            if (this.groups.length === 0) {
                this.createGroups([
                    {
                        id: 1,
                    },
                    {
                        id: 2,
                    },
                    {
                        id: 3,
                    },
                    {
                        id: 4,
                    },
                ]);
            }

            this.players = playersConfig.reduce((acc, pConfig): Array<Player> => {
                const createdPlayer = new Player(
                    pConfig.name,
                    pConfig.id,
                    validate(
                        this.groups.find((g) => g.getId() === pConfig.groupId),
                        new Error(
                            `Group '${pConfig.id}' not found in Player '${pConfig.id}' configuration.`,
                        ),
                    ),
                    buttonMatrix,
                    validate(
                        controllers.find((c) => c.id === pConfig.controllerId),
                        new Error(
                            `Controller ${pConfig.controllerId} not found in Player '${pConfig.id}' configuration.`,
                        ),
                    ).button, // ids of button [up, left, right, catch]
                );
                this.getScoreBoard().addPlayer(createdPlayer);

                this.getEventEmitter().emit(GameEvents.ON_PLAYER_CREATED, createdPlayer);
                acc.push(createdPlayer);
                return acc;
            }, new Array<Player>());
            console.log(`Player after : ${this.players}`);

            // Setting model on intersection segments
            LOGGER.debug(`\t\t- Setting up intersections and groups`);
            playersConfig.forEach((pConfig) => {
                const player = validate(this.players.find((p) => p.getId() === pConfig.id));
                const intersection = validate(
                    this.getIntersections().find((i) => i.getId() === pConfig.intersectionId),
                );

                LOGGER.debug(`\t\t\t- Adding model to intersection '${pConfig.intersectionId}'`);
                intersection.setModel(pConfig.model);

                if (pConfig.groupId !== undefined) {
                    LOGGER.debug(`\t\t\t- Adding group '${pConfig.groupId}'`);
                    const group = validate(this.groups.find((g) => g.getId() === pConfig.groupId));

                    LOGGER.debug(
                        `\t\t\t\t- Link group '${pConfig.groupId}' to player '${pConfig.id}'`,
                    );
                    player.setGroup(group);

                    LOGGER.debug(
                        `\t\t\t\t- Link group '${pConfig.groupId}' to intersection '${pConfig.intersectionId}'`,
                    );
                    intersection.addGroup(group);
                }
            });

            resolve();
        });
    }

    public createBalls(ballConfig: Array<GameModeBall>): void {
        LOGGER.info(`\t- Creating '${ballConfig.length}' ball(s)`);
        ballConfig.forEach((bConfig) => {
            const createdBall = validate(
                this.gameCores.segments.find((s) => s.getId() === bConfig.segmentId),
                new Error(
                    `Segment '${bConfig.segmentId}' not found in Ball '${bConfig.id}' configuration`,
                ),
            )?.createBall(
                bConfig.id,
                bConfig.model,
                bConfig.initialSpeed,
                bConfig.direction,
                bConfig.offsetPosition ?? 0,
                this.mode.config.ballRedirection,
            );

            if (bConfig.groupId !== undefined) {
                LOGGER.debug(`\t\t- Adding group '${bConfig.groupId}'`);
                const group = validate(this.groups.find((g) => g.getId() === bConfig.groupId));
                createdBall.setGroup(group);
            }

            this.ballsId.push(bConfig.id);
            this.getEventEmitter().emit(GameEvents.ON_BALL_CREATED, createdBall);
            createdBall.startMove();
            this.getEventEmitter().emit(GameEvents.ON_BALL_MOVING, createdBall);
        });
    }

    public removeBalls(...ids: Array<number>) {
        this.ballsId = this.ballsId.filter((ballId) => !ids.includes(ballId));
        this.getSegments().forEach((segment) => {
            segment.removeBalls(...this.ballsId);
        });
    }

    public animate(element: Animable, pattern: Array<number>, times: Array<number>): void {
        element.animate(new Uint32Array(pattern), times);
    }

    public define(property: GameProperty, value: unknown, arg?: unknown): void {
        this.gameProperties.set(property, { value, arg });
    }

    private resumeGameProperties() {
        const displayRotationProperties = (
            this.gameProperties.get(GameProperty.S_DISPLAY_ROTATION)
                ?.value as Array<DisplayRotationProperties>
        ).map((display) => display.display);
        const gameType = this.mode.config.type === GameType.SCORE ? 'Score' : 'Life Count';

        LOGGER.debug('');
        LOGGER.debug('-> ----------- Cores Resume ------------');
        LOGGER.debug(`-> Board :`);
        LOGGER.debug(`-> \tPixels Length : ${this.gameCores.board.getPixels().length}`);
        LOGGER.debug(`-> Segments (count: ${this.getSegments().length}) :`);
        this.getSegments().forEach((s) => {
            LOGGER.debug(`-> \t${s.toString()}`);
        });
        LOGGER.debug(`-> Intersections (count: ${this.getIntersections().length}) :`);
        this.getIntersections().forEach((i) => {
            LOGGER.debug(`-> \t${i.toString()}`);
        });
        LOGGER.debug(`-> Controllers (count: ${this.gameCores.controllers.length}) :`);
        LOGGER.debug(
            `-> Serial Port : ${this.gameCores.serialPort.path} - ${this.gameCores.serialPort.baudRate}`,
        );
        LOGGER.debug('');
        LOGGER.debug('-> ----------- Game Resume ------------');
        LOGGER.debug(`-> Name: ${this.mode.config.name}`);
        LOGGER.debug(`-> Players count : ${this.players.length}`);
        LOGGER.debug(`-> Game Properties : `);
        LOGGER.debug(`-> \t- Game Type: ${gameType}`);
        LOGGER.debug(`-> \t- Duration Factor: ${this.mode.config.durationFactor}`);
        LOGGER.debug(`-> \t- Max Duration: ${this.mode.config.maxDuration}`);
        LOGGER.debug(`-> \t- Final Duration: ${this.getGameDuration()}`);
        LOGGER.debug(`-> \t- Max score: ${this.mode.config.maxScore}`);
        LOGGER.debug(`-> \t- Display Rotation: [${displayRotationProperties}]`);
        LOGGER.debug('');
    }

    private manageEndOfGame(): void {
        if (this.gameProperties.size === undefined) return;

        let stopReason = undefined;

        // Check game time
        const gameTimeProperty = this.getGameDuration();
        if (
            gameTimeProperty !== undefined &&
            gameTimeProperty * 1000 - this.getRunningTimeMs() <= 0
        ) {
            stopReason = GameStoppedReason.TIME_OVER;
        }

        if (this.mode.config.type === GameType.LIFE) {
            const lifeCount = this.mode.config.maxScore;
            this.players.forEach((player) => {
                if (player.getScore().getValue() >= lifeCount) {
                    player.setEliminated(true);
                    this.getEventEmitter().emit(GameEvents.ON_PLAYER_ELIMINATED, player);
                }
            });

            if (this.players.filter((p) => p.isEliminated()).length === this.players.length) {
                stopReason = GameStoppedReason.ALL_PLAYERS_ARE_ELIMINATED;
            }
        } else if (this.mode.config.type === GameType.SCORE) {
            const scoreValueLimit = this.mode.config.maxScore;
            if (
                scoreValueLimit !== undefined &&
                this.players.filter((p) => p.getScore().getValue() >= scoreValueLimit).length > 0
            ) {
                stopReason = GameStoppedReason.PLAYER_REACH_MAX_SCORE;
            }
        }

        if (stopReason !== undefined) this.stop(stopReason);
    }

    private handlePlayerButtonPressed(player: Player, btnType: ButtonType): void {
        this.getEventEmitter().emit(GameEvents.ON_PLAYER_CLICK, player, btnType);

        if (player.hasInteractions(PlayerInteractions.PLAYER_CLICKED)) return;
        player.setInteractions(PlayerInteractions.PLAYER_CLICKED);

        const playerIntersections = this.getIntersections().filter((i) => {
            const playerGroup = player.getGroup();
            if (playerGroup === undefined) return true;
            if (i.getGroups().length === 0) return false;
            return i.getGroupsId().includes(playerGroup.getId());
        });

        const intersectionBalls = playerIntersections.reduce((acc, playerIntersection) => {
            playerIntersection
                .getBalls(player)
                .forEach((intersectionAreaType, ball) =>
                    acc.set(ball, { playerIntersection, intersectionAreaType }),
                );
            return acc;
        }, new Map<Ball, { playerIntersection: Intersection; intersectionAreaType: IntersectionAreaType }>());

        if (intersectionBalls.size === 0) {
            this.getEventEmitter().emit(
                GameEvents.ON_PLAYER_CLICK_OUTSIDE_AREAS,
                player,
                playerIntersections,
            );
        } else {
            intersectionBalls.forEach(({ playerIntersection, intersectionAreaType }, ball) => {
                if (!ball.isAlreadyRedirected()) {
                    player.disableBallRedirection();
                    this.getEventEmitter().emit(
                        GameEvents.ON_PLAYER_CLICK_IN_HIS_AREA,
                        player,
                        ball,
                        playerIntersection,
                    );
                    ball.addInteraction(BallInteractions.BALL_CLICKED);
                    if (intersectionAreaType === IntersectionAreaType.ACTIVE_AREA) {
                        this.getEventEmitter().emit(
                            GameEvents.ON_PLAYER_CLICK_IN_ACTIVE_AREA,
                            player,
                            ball,
                            playerIntersection,
                            btnType,
                        );
                    } else {
                        this.getEventEmitter().emit(
                            GameEvents.ON_PLAYER_CLICK_IN_PASSIVE_AREA,
                            player,
                            playerIntersection,
                        );
                    }
                } else {
                    this.getEventEmitter().emit(
                        GameEvents.ON_PLAYER_CLICK_MULTIPLE_TIMES_IN_AREA,
                        player,
                        playerIntersection,
                    );
                }
            });
        }

        player.clearInteractions();
    }

    private handlePlayerButtonRelease(player: Player, btnType: ButtonType): void {
        this.getEventEmitter().emit(GameEvents.ON_PLAYER_CLICK_RELEASE, player, btnType);
    }

    private handleIntersectionBallLeaving(ball: Ball, intersection: Intersection): void {
        if (intersection.getGroups().length === 0) return;

        if (
            !ball.hasInteractions(BallInteractions.LEAVING_AREA) &&
            ball.hasInteractions(BallInteractions.ENTERING_AREA)
        ) {
            if (!ball.hasInteractions(BallInteractions.BALL_CLICKED)) {
                this.getEventEmitter().emit(
                    GameEvents.ON_BALL_LEAVING_AREA_WITHOUT_INTERACTIONS,
                    ball,
                    intersection,
                );
                ball.addInteraction(BallInteractions.LEAVING_AREA);
            }
            ball.clearInteractions();
        }
    }

    private handleIntersectionBallEntering(ball: Ball, intersection: Intersection): void {
        if (intersection.getGroups().length === 0) return;

        this.getEventEmitter().emit(GameEvents.ON_BALL_ENTERING_AREA, ball, intersection);
        ball.addInteraction(BallInteractions.ENTERING_AREA);
    }

    public on(
        event: GameEvents,
        callbacks: {
            [difficulty in GameDifficulty | 'ALL']?: (...args: any) => void; // eslint-disable-line @typescript-eslint/no-explicit-any
        },
    ): void {
        if (Object.entries(callbacks).length === 0)
            throw new Error('No callback given in event listener');

        const callbackGlobalFunction = Object.entries(callbacks).find(
            (difficulty) => difficulty[0] === 'ALL',
        );

        if (callbackGlobalFunction !== undefined) {
            LOGGER.debug(`\t- Setting up event for all difficulties : ${event}`);
            this.getEventEmitter().on(event, callbackGlobalFunction[1]);
        }

        const callbackByDifficultyFunctions = Object.entries(callbacks).find(
            (difficulty) => (difficulty[0] as GameDifficulty) === this.difficulty,
        );

        if (callbackByDifficultyFunctions !== undefined) {
            LOGGER.debug(
                `\t- Setting up event for difficulty [${callbackByDifficultyFunctions[0]}] : ${event}`,
            );
            this.getEventEmitter().on(event, callbackByDifficultyFunctions[1]);
        }

        if (callbackGlobalFunction === undefined && callbackByDifficultyFunctions === undefined)
            throw new Error(
                `No callback function found for the selected difficulty '${
                    GameDifficulty[this.difficulty as keyof typeof GameDifficulty]
                }' event listener '${event}'`,
            );
    }

    public emit(event: GameEvents, ...args: Array<unknown>): void {
        this.getEventEmitter().emit(event, this.gameCores, args);
    }

    public await(
        event: GameEvents,
        timeout = 1000,
        immediateCallback = (): void => {
            return;
        },
    ): Promise<void> {
        return new Promise((resolve, reject) => {
            immediateCallback();
            let timeoutId: NodeJS.Timeout | undefined = undefined;

            const awaitHandlerFunction = () => {
                this.getEventEmitter().removeListener(event, awaitHandlerFunction);
                if (timeoutId !== undefined) clearTimeout(timeoutId);
                resolve();
            };

            this.getEventEmitter().on(event, awaitHandlerFunction);

            timeoutId = setTimeout(() => {
                this.getEventEmitter().removeListener(event, awaitHandlerFunction);
                reject();
            }, timeout);
        });
    }

    public getBoard(): Board {
        return this.gameCores.board;
    }

    public getEventEmitter(): EventEmitter {
        return validate(this.event, new Error('Event listener not found'));
    }

    public getPlayers(): Array<Player> {
        return this.players;
    }

    public getBalls(): Array<Ball> {
        return this.gameCores.segments.reduce((acc, segment): Array<Ball> => {
            acc.concat(segment.getBalls(...this.ballsId));
            return acc;
        }, new Array<Ball>());
    }

    public getSegments(...ids: Array<number>): Array<Segment> {
        if (ids.length === 0) return this.gameCores.segments;
        else return this.gameCores.segments.filter((s) => ids.includes(s.getId()));
    }

    public getIntersections(groupId?: number): Array<Intersection> {
        if (groupId === undefined) return this.gameCores.intersections;
        return this.gameCores.intersections.filter((intersection) =>
            intersection
                .getGroups()
                .map((g) => g.getId())
                .includes(groupId),
        );
    }

    public getMode(): GameMode {
        return this.mode;
    }

    public getStore(): Map<unknown, unknown> {
        return this.store;
    }

    public getScoreBoard(): ScoreBoard {
        return validate(this.gameCores.scoreboard);
    }

    public getGameDuration(): number {
        if (this.players.length === 0) return 0;
        return Math.floor(
            this.mode.config.maxDuration -
                this.mode.config.maxDuration *
                    (this.players.length - 1) *
                    this.mode.config.durationFactor,
        );
    }

    public getGameTimeMs(): number {
        return this.getRunningTimeMs();
    }

    public getGameTimeLeftMs(): number {
        return Math.floor(this.getGameDuration() * 1000 - this.getGameTimeMs());
    }

    public getStatus(): RunnableStatus {
        return this.status;
    }

    public getMetaData(): GameMetaData {
        const time =
            this.mode.config.type === GameType.SCORE
                ? this.getGameTimeLeftMs()
                : this.getGameTimeMs();
        const totalDuration = this.getGameDuration();
        const status = this.getStatus();
        const playerList = this.getPlayers().map((p) => {
            return {
                id: p.getId(),
                name: p.getName(),
                rank: this.getScoreBoard().getPlayerRank(p),
                group: p.getGroup().id,
                score: p.getScore().getValue(),
            };
        });
        const alivePlayerCount = this.players.filter((p) => !p.isEliminated()).length;

        return {
            time: time,
            duration: totalDuration,
            type: this.mode.config.type,
            status: status,
            count: {
                ball: this.ballsId.length,
                alivePlayer: alivePlayerCount,
                eliminatedPlayer: this.players.length - alivePlayerCount,
            },
            players: playerList,
        };
    }

    public isBallRedirectionEnable(): boolean {
        return this.mode.config.ballRedirection;
    }

    private isAnimationsInProgress(): boolean {
        let animationInProgress = false;
        this.getSegments().forEach((s) => {
            if (s.isAnimationsInProgress()) {
                console.log(`==== Segment: ${s.getId()} : ${s.isAnimationsInProgress()} ====`);
                animationInProgress = true;
            }
        });
        this.getIntersections().forEach((i) => {
            if (i.isAnimationsInProgress()) {
                console.log(`==== Intersection: ${i.getId()} ====`);
                animationInProgress = true;
            }
        });

        return animationInProgress;
    }
}
