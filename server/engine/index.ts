import Cores from './Cores';
import Game from './Game';
import ScoreBoard from './ScoreBoard';

export { Cores, Game, ScoreBoard };
