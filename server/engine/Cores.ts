import { SerialPort } from 'serialport';
import { exec } from 'child_process';

import ButtonMatrix from '../core/ButtonMatrix';
import Board from '../core/Board';

import { AppConfig, Controller, Cores as CoresObject, SegmentNeighbor } from '../interfaces';
import { Segment, Intersection } from '../entity';

import { SegmentPosition } from '../enums';
import { validate } from '../utils/utils';
import { Logger } from '../service/Logger';

const LOGGER = Logger.getInstance();

export class Cores {
    private gameMapping!: AppConfig;
    private emulated!: boolean;
    private board!: Board;
    private segments!: Array<Segment>;
    private intersections!: Array<Intersection>;
    private buttonMatrix!: ButtonMatrix;
    private controllers!: Array<Controller>;
    private serialPort!: SerialPort;

    /**
     * Create board and turn all led on for visual check (turn it back off after 3s)
     *
     * @returns the game board
     */
    private initBoard(): Promise<Board> {
        return new Promise((resolve) => {
            LOGGER.info('\t- Creating board');
            const board = new Board(this.gameMapping.board);
            resolve(board);
        });
    }

    /**
     * Create segments and store them into a map
     *
     * @returns the segments maps <id, Segment>
     */
    private initSegments(): Promise<Array<Segment>> {
        return new Promise(async (resolve) => {
            LOGGER.info('\t- Creating Segments');
            const segments = await this.gameMapping.segments.list.reduce(
                async (prevPromise, segmentOptions): Promise<Array<Segment>> => {
                    const acc = await prevPromise;
                    return new Promise((resolve) => {
                        const segment = new Segment(
                            segmentOptions.id,
                            segmentOptions.indices,
                            this.board,
                        );
                        setTimeout(() => {
                            const areaId = segment.addStaticArea(
                                new Uint32Array(segmentOptions.indices.length).fill(0x808080),
                            );
                            segment.draw(this.board);
                            this.board.flush();
                            segment.removeStaticArea(areaId);
                            acc.push(segment);
                            resolve(acc);
                        }, 100);
                    });
                },
                Promise.resolve(new Array<Segment>()),
            );
            setTimeout(() => {
                this.board.clear();
                this.board.flush();
                resolve(segments);
            }, 100);
        });
    }

    /**
     * Create all intersections
     */
    private initIntersections(): Promise<Array<Intersection>> {
        return new Promise((resolve) => {
            LOGGER.info('\t- Creating Intersections');
            const intersections = this.gameMapping.intersections.list.reduce(
                (acc, intersectionOptions) => {
                    const intersectionNeighbors = intersectionOptions.segments.reduce(
                        (acc, segmentInfo) => {
                            const neighborSegment = validate(
                                this.segments.find((s) => s.getId() === segmentInfo.id),
                            );
                            acc.push({
                                reference: neighborSegment,
                                direction: segmentInfo.position,
                                buttonType: segmentInfo.buttonMapping,
                            });
                            return acc;
                        },
                        new Array<SegmentNeighbor>(),
                    );

                    const intersection = new Intersection(
                        intersectionOptions.id,
                        intersectionNeighbors,
                        this.board,
                    );
                    acc.push(intersection);
                    return acc;
                },

                new Array<Intersection>(),
            );

            LOGGER.debug('\t\t- Mapping Intersections');
            this.gameMapping.segments.list.forEach((segmentOptions) => {
                validate(this.segments.find((s) => s.getId() === segmentOptions.id))
                    .setIntersection(
                        SegmentPosition.BEGIN,
                        validate(
                            intersections.find(
                                (i) => i.getId() === segmentOptions.beginIntersection,
                            ),
                        ),
                    )
                    .setIntersection(
                        SegmentPosition.END,
                        validate(
                            intersections.find((i) => i.getId() === segmentOptions.endIntersection),
                        ),
                    );
            });

            resolve(intersections);
        });
    }

    /**
     * Create Button Matrix
     *
     * @returns the button matrix
     */
    private initButtonMatrix(): Promise<ButtonMatrix> {
        return new Promise((resolve) => {
            LOGGER.info('\t- Creating ButtonMatrix');
            const buttonMatrix = new ButtonMatrix(this.gameMapping.buttonMatrix, this.emulated);
            resolve(buttonMatrix);
        });
    }

    /**
     * Create controllers
     *
     * @returns the controllers map <id, Controller>
     */
    private initControllers(): Promise<Array<Controller>> {
        LOGGER.info('\t- Creating Controllers');
        return new Promise((resolve) => {
            const controllers = this.gameMapping.controllers.list.reduce(
                (acc, controllerConfig) => {
                    LOGGER.debug(`\t\t- Initialize controller ${controllerConfig.id}`);
                    acc.push({
                        id: controllerConfig.id,
                        button: controllerConfig.button,
                    });
                    return acc;
                },
                new Array<Controller>(),
            );
            resolve(controllers);
        });
    }

    /**
     * Init serial port
     *
     * @returns the controllers map <id, Controller>
     */
    private initSerialPort(): Promise<SerialPort> {
        return new Promise(async (resolve) => {
            LOGGER.info('\t- Initialize SerialPort');
            const serial = new SerialPort(this.gameMapping.serialPort);
            const appVersion = (process.env.npm_package_version ?? '1.0')
                .split('.')
                .slice(0, 2)
                .join('.');
            serial.write(`!3${''.padEnd(24, '*')}"`);
            serial.write(`!9${appVersion.padEnd(24, '*')}"`, (err) => {
                if (err) throw err;
                setTimeout(() => {
                    serial.write(`!3${''.padEnd(24, '*')}"`);
                    resolve(serial);
                }, 1000);
            });
        });
    }

    /**
     * Check if there is an HDMI screen connected
     */
    private initHdmi(): Promise<void> {
        return new Promise(async (resolve) => {
            LOGGER.info('\t\t- Checking main screen');
            exec(
                "xrandr --listmonitors | awk -F'Monitors: ' 'NR==1 {print $2}'",
                (_err, stdout) => {
                    if (Number(stdout) != 1) throw new Error('No connected HDMI screen found');
                },
            );
            resolve();
        });
    }

    /**
     * Launch web user interface
     */
    private initBrowser(): Promise<void> {
        return new Promise(async (resolve) => {
            LOGGER.info('\t\t- Launching web user interface');
            exec('chromium-browser --start-fullscreen --app=http://127.0.0.1:3000/', (err) => {
                if (err) throw new Error(`Unable to launch browser : ${err.message}`);
            });
            resolve();
        });
    }

    public async getCores(gameMapping: AppConfig): Promise<CoresObject> {
        this.gameMapping = gameMapping;
        this.emulated = gameMapping.emulated;

        this.board = await this.initBoard();
        this.segments = await this.initSegments();
        this.intersections = await this.initIntersections();
        this.buttonMatrix = await this.initButtonMatrix();
        this.controllers = await this.initControllers();
        this.serialPort = await this.initSerialPort();

        // await this.initHdmi();
        // await this.initBrowser();

        return {
            board: this.board,
            segments: this.segments,
            intersections: this.intersections,
            buttonMatrix: this.buttonMatrix,
            controllers: this.controllers,
            serialPort: this.serialPort,
        };
    }
}

export default Cores;
