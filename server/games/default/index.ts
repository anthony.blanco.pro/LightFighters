import { GameMode } from '../../interfaces';

import gameConfig from './config';
import gameMain from './main';

export const game: GameMode = {
    config: gameConfig,
    main: gameMain,
};
