import { GameDifficulty, GameType } from '../../enums';
import { GameModeConfig } from '../../interfaces';

export default {
    name: 'Classique',
    author: "Lightfighters Team's",
    type: GameType.SCORE,
    description:
        'Le jeu sera composé de plusieurs balles pouvant être redirigé par un joueur si ' +
        'celle-ci se situe dans sa zone de touche. La vitesse de la balle augmente progressivement selon la difficulté ' +
        "choisie. Le jeu prendra fin si l'un des joueurs présent obtient un score de 400 points ou à la fin du temps " +
        'imparti (1 à 2 min selon la difficulté et le nombre de joueur choisie).' +
        '\n- Une balle est representé par plusieurs led blanche en mouvement qui apparaissent au debut de la partie. ' +
        "\n- La zone de touche d'un joueur est representé par plusieurs led allumées et colorées au dessus de ses boutons" +
        "\n- Les boutons : Gauche, Droit et Haut permettent de rediriger la balle lorsqu'elle se situe dans votre zone de touche peu importe d'ou la balle arrive" +
        "\n- Le bouton : Bas permet d'accélerer plus fortement la vitesse de la balle (il n'est pas possible de re-diriger puis d'accélerer sa vitesse ou inverssement)\n" +
        "Lors d'une touche de la balle (appuie sur un bouton et qu'une balle se trouve dans votre zone) la" +
        "balle clignotera en vert (score augmenté). Lorsqu'une balle traverse une zone de touche et que le joueur n'appui" +
        "pas sur l'un de ses boutons ou lors d'un appui et qu'il n'y a pas de balle de presente, son score diminuera." +
        '' +
        'Bon jeu !',
    minPlayer: 1,
    maxPlayer: 4,
    ballRedirection: true,
    defaultDifficulty: GameDifficulty.NORMAL,
    groupAllowed: false,
    imageUrl: '',
    maxDuration: 120,
    durationFactor: 0.1, // duration = maxDuration - ((playerCount - 1) * maxDuration * durationFactor)
    maxScore: 400,
} as GameModeConfig;
