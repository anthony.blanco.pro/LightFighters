import { Game } from '../../engine';
import { Ball, Intersection, Player } from '../../entity';
import {
    GameEvents as Events,
    BallDirection,
    GameProperty,
    ButtonType,
    DisplayLayout,
    PlayerInteractions,
} from '../../enums';

export default function gameMain(g: Game): void {
    g.define(GameProperty.S_DISPLAY_ROTATION, [
        {
            priority: 1,
            display: DisplayLayout.SCORE,
            duration: 4000,
        },
        {
            priority: 2,
            display: DisplayLayout.TIME_LEFT,
            duration: 3000,
        },
        {
            priority: 3,
            display: DisplayLayout.PERSONAL_RANK,
            duration: 2500,
        },
    ]);

    g.on(Events.BEFORE_INIT, {
        EASY: () => {
            g.createBalls([
                {
                    id: 1,
                    segmentId: 10,
                    model: new Uint32Array([0xffffff, 0xe2e2e2, 0xc1c1c1, 0x797979, 0x474747]),
                    initialSpeed: 10,
                    direction: BallDirection.FORWARD,
                },
                {
                    id: 1,
                    segmentId: 10,
                    model: new Uint32Array([0xffffff, 0xe2e2e2, 0xc1c1c1, 0x797979, 0x474747]),
                    initialSpeed: 10,
                    direction: BallDirection.FORWARD,
                },
            ]);
        },
        NORMAL: () => {
            g.createBalls([
                {
                    id: 1,
                    segmentId: 10,
                    model: new Uint32Array([0xffffff, 0xe2e2e2, 0xc1c1c1, 0x797979, 0x474747]),
                    initialSpeed: 10,
                    direction: BallDirection.FORWARD,
                },
                {
                    id: 2,
                    segmentId: 10,
                    model: new Uint32Array([0xffffff, 0xe2e2e2, 0xc1c1c1, 0x797979, 0x474747]),
                    initialSpeed: 10,
                    direction: BallDirection.FORWARD,
                },
                {
                    id: 3,
                    segmentId: 10,
                    model: new Uint32Array([0xffffff, 0xe2e2e2, 0xc1c1c1, 0x797979, 0x474747]),
                    initialSpeed: 10,
                    direction: BallDirection.FORWARD,
                },
            ]);
        },
        HARD: () => {
            g.createBalls([
                {
                    id: 1,
                    segmentId: 10,
                    model: new Uint32Array([0xffffff, 0xe2e2e2, 0xc1c1c1, 0x797979, 0x474747]),
                    initialSpeed: 10,
                    direction: BallDirection.FORWARD,
                },
                {
                    id: 2,
                    segmentId: 10,
                    model: new Uint32Array([0xffffff, 0xe2e2e2, 0xc1c1c1, 0x797979, 0x474747]),
                    initialSpeed: 10,
                    direction: BallDirection.FORWARD,
                },
                {
                    id: 3,
                    segmentId: 10,
                    model: new Uint32Array([0xffffff, 0xe2e2e2, 0xc1c1c1, 0x797979, 0x474747]),
                    initialSpeed: 10,
                    direction: BallDirection.FORWARD,
                },
                {
                    id: 4,
                    segmentId: 10,
                    model: new Uint32Array([0xffffff, 0xe2e2e2, 0xc1c1c1, 0x797979, 0x474747]),
                    initialSpeed: 10,
                    direction: BallDirection.FORWARD,
                },
            ]);
        },
    });

    g.on(Events.ON_TICK, {
        ALL: () => {
            const totalScore = g
                .getPlayers()
                .reduce((sum, p): number => sum + p.getScore().getValue(), 0);

            if (g.getStore().get('oldTotalScore') === undefined)
                g.getStore().set('oldTotalScore', -1);
            g.getStore().set('totalScore', totalScore);

            if (
                g.getStore().get('oldTotalScore') !== g.getStore().get('totalScore') &&
                totalScore != 0 &&
                (totalScore === 50 || totalScore === 90 || totalScore === 120 || totalScore === 160)
            ) {
                g.getStore().set('oldTotalScore', totalScore);
                g.createBalls([
                    {
                        id: 1,
                        segmentId: 10,
                        model: new Uint32Array([0xffffff, 0xe2e2e2, 0xc1c1c1, 0x797979, 0x474747]),
                        initialSpeed: 18,
                        direction: BallDirection.FORWARD,
                    },
                ]);
            }
        },
    });

    g.on(Events.ON_BALL_LEAVING_AREA_WITHOUT_INTERACTIONS, {
        ALL: (ball: Ball, intersection: Intersection) => {
            g.getPlayers().forEach((p) => {
                const playerGroupId = p.getGroup()?.getId();
                if (playerGroupId === undefined) return;
                if (intersection.getGroupsId().includes(playerGroupId)) {
                    p.getScore().computeValue(-7);
                }
            });
        },
    });

    g.on(Events.ON_PLAYER_CLICK_IN_HIS_AREA, {
        ALL: (player: Player, ball: Ball, _, btnType: ButtonType) => {
            player.setInteractions(PlayerInteractions.PLAYER_CLICKED);
            if (g.isBallRedirectionEnable() && btnType !== ButtonType.CATCH) {
                player.getScore().computeValue(10);
            }

            const animationModel = [0x00fe00, 0xffffff, 0x00fe00, 0xffffff, 0x00fe00, 0xffffff];
            const animationTiming = [200, 100, 200, 100, 200, 100];
            g.animate(ball, animationModel, animationTiming);
        },
    });

    g.on(Events.ON_PLAYER_CLICK_IN_ACTIVE_AREA, {
        ALL: (
            _player: Player,
            ball: Ball,
            playerIntersection: Intersection,
            btnType: ButtonType,
        ) => {
            if (
                g.isBallRedirectionEnable() &&
                [ButtonType.LEFT, ButtonType.UP, ButtonType.RIGHT].includes(btnType)
            ) {
                ball.setNextNeighbor(playerIntersection.getNeighborByButtonType(btnType));
                ball.setSpeed(ball.getSpeed() + 2);
            } else if (g.isBallRedirectionEnable() && btnType === ButtonType.CATCH) {
                ball.setSpeed(ball.getSpeed() + 4);
            }
        },
    });

    g.on(Events.ON_PLAYER_CLICK_OUTSIDE_AREAS, {
        ALL: (player: Player, intersections: Array<Intersection>) => {
            const animationModel = [0xff3300, 0x000000, 0xff3300, 0x000000];
            const animationTiming = [100, 50, 100, 50];
            player.getScore().computeValue(-5);
            intersections.forEach((i) => g.animate(i, animationModel, animationTiming));
        },
    });

    g.on(Events.ON_STOP, {
        ALL: (): void => {
            g.getScoreBoard().setDisplay(DisplayLayout.RANKS, true);
            g.getSegments().forEach((segment) => {
                g.animate(segment, [0xafafaf, 0x000, 0xafafaf, 0x000], [500, 500, 500, 500]);
            });
        },
    });
}
