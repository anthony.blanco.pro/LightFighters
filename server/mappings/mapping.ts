import { AppConfig } from '../interfaces';
import { ButtonType, SegmentPosition } from '../enums';

const mappingProd: AppConfig = {
    emulated: false,
    board: {
        leds: 233,
        gpio: 18,
        stripType: 'grb',
        dma: 10,
        brightness: 100,
    },
    buttonMatrix: {
        gpioPins: {
            x: [19, 23, 24, 25],
            y: [12, 16, 20, 21],
        },
    },
    serialPort: {
        path: '/dev/ttyAMA0',
        baudRate: 9600,
        autoOpen: true,
    },
    segments: {
        list: [
            {
                id: 1,
                indices: [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12],
                beginIntersection: 1,
                endIntersection: 2,
            },
            {
                id: 2,
                indices: [13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25],
                beginIntersection: 2,
                endIntersection: 3,
            },
            {
                id: 3,
                indices: [26, 27, 28, 29, 30, 31, 32, 33, 34, 35, 36, 37, 38],
                beginIntersection: 3,
                endIntersection: 4,
            },
            {
                id: 4,
                indices: [39, 40, 41, 42, 43, 44, 45, 46, 47, 48, 49, 50, 51],
                beginIntersection: 4,
                endIntersection: 5,
            },
            {
                id: 5,
                indices: [52, 53, 54, 55, 56, 57, 58, 59, 60, 61, 62, 63, 64],
                beginIntersection: 5,
                endIntersection: 6,
            },
            {
                id: 6,
                indices: [65, 66, 67, 68, 69, 70, 71, 72, 73, 74, 75, 76, 77],
                beginIntersection: 6,
                endIntersection: 7,
            },
            {
                id: 7,
                indices: [78, 79, 80, 81, 82, 83, 84, 85, 86, 87, 88, 89, 90],
                beginIntersection: 7,
                endIntersection: 8,
            },
            {
                id: 8,
                indices: [91, 92, 93, 94, 95, 96, 97, 98, 99, 100, 101, 102, 103],
                beginIntersection: 8,
                endIntersection: 1,
            },
            {
                id: 9,
                indices: [
                    104, 105, 106, 107, 108, 109, 110, 111, 112, 113, 114, 115, 116, 117, 118, 119,
                ],
                beginIntersection: 1,
                endIntersection: 9,
            },
            {
                id: 10,
                indices: [120],
                beginIntersection: 0,
                endIntersection: 0,
            },
            {
                id: 11,
                indices: [
                    121, 122, 123, 124, 125, 126, 127, 128, 129, 130, 131, 132, 133, 134, 135, 136,
                ],
                beginIntersection: 10,
                endIntersection: 2,
            },
            {
                id: 12,
                indices: [
                    137, 138, 139, 140, 141, 142, 143, 144, 145, 146, 147, 148, 149, 150, 151, 152,
                ],
                beginIntersection: 3,
                endIntersection: 11,
            },
            {
                id: 13,
                indices: [
                    153, 154, 155, 156, 157, 158, 159, 160, 161, 162, 163, 164, 165, 166, 167, 168,
                ],
                beginIntersection: 12,
                endIntersection: 4,
            },
            {
                id: 14,
                indices: [
                    169, 170, 171, 172, 173, 174, 175, 176, 177, 178, 179, 180, 181, 182, 183, 184,
                ],
                beginIntersection: 5,
                endIntersection: 13,
            },
            {
                id: 15,
                indices: [
                    185, 186, 187, 188, 189, 190, 191, 192, 193, 194, 195, 196, 197, 198, 199, 200,
                ],
                beginIntersection: 14,
                endIntersection: 6,
            },
            {
                id: 16,
                indices: [
                    201, 202, 203, 204, 205, 206, 207, 208, 209, 210, 211, 212, 213, 214, 215, 216,
                ],
                beginIntersection: 7,
                endIntersection: 15,
            },
            {
                id: 17,
                indices: [
                    217, 218, 219, 220, 221, 222, 223, 224, 225, 226, 227, 228, 229, 230, 231, 232,
                ],
                beginIntersection: 16,
                endIntersection: 8,
            },
        ],
    },
    intersections: {
        list: [
            {
                id: 0,
                segments: [
                    {
                        id: 9,
                        position: SegmentPosition.END,
                    },
                    {
                        id: 11,
                        position: SegmentPosition.BEGIN,
                    },
                    {
                        id: 12,
                        position: SegmentPosition.END,
                    },
                    {
                        id: 13,
                        position: SegmentPosition.BEGIN,
                    },
                    {
                        id: 14,
                        position: SegmentPosition.END,
                    },
                    {
                        id: 15,
                        position: SegmentPosition.BEGIN,
                    },
                    {
                        id: 16,
                        position: SegmentPosition.END,
                    },
                    {
                        id: 17,
                        position: SegmentPosition.BEGIN,
                    },
                ],
            },
            {
                id: 1,
                segments: [
                    {
                        id: 8,
                        position: SegmentPosition.END,
                        buttonMapping: ButtonType.RIGHT,
                    },
                    {
                        id: 9,
                        position: SegmentPosition.BEGIN,
                        buttonMapping: ButtonType.UP,
                    },
                    {
                        id: 1,
                        position: SegmentPosition.BEGIN,
                        buttonMapping: ButtonType.LEFT,
                    },
                ],
            },
            {
                id: 2,
                segments: [
                    {
                        id: 1,
                        position: SegmentPosition.END,
                        buttonMapping: ButtonType.RIGHT,
                    },
                    {
                        id: 11,
                        position: SegmentPosition.END,
                        buttonMapping: ButtonType.UP,
                    },
                    {
                        id: 2,
                        position: SegmentPosition.BEGIN,
                        buttonMapping: ButtonType.LEFT,
                    },
                ],
            },
            {
                id: 3,
                segments: [
                    {
                        id: 2,
                        position: SegmentPosition.END,
                        buttonMapping: ButtonType.RIGHT,
                    },
                    {
                        id: 12,
                        position: SegmentPosition.BEGIN,
                        buttonMapping: ButtonType.UP,
                    },
                    {
                        id: 3,
                        position: SegmentPosition.BEGIN,
                        buttonMapping: ButtonType.LEFT,
                    },
                ],
            },
            {
                id: 4,
                segments: [
                    {
                        id: 3,
                        position: SegmentPosition.END,
                        buttonMapping: ButtonType.RIGHT,
                    },
                    {
                        id: 13,
                        position: SegmentPosition.END,
                        buttonMapping: ButtonType.UP,
                    },
                    {
                        id: 4,
                        position: SegmentPosition.BEGIN,
                        buttonMapping: ButtonType.LEFT,
                    },
                ],
            },
            {
                id: 5,
                segments: [
                    {
                        id: 4,
                        position: SegmentPosition.END,
                        buttonMapping: ButtonType.RIGHT,
                    },
                    {
                        id: 14,
                        position: SegmentPosition.BEGIN,
                        buttonMapping: ButtonType.UP,
                    },
                    {
                        id: 5,
                        position: SegmentPosition.BEGIN,
                        buttonMapping: ButtonType.LEFT,
                    },
                ],
            },
            {
                id: 6,
                segments: [
                    {
                        id: 5,
                        position: SegmentPosition.END,
                        buttonMapping: ButtonType.RIGHT,
                    },
                    {
                        id: 15,
                        position: SegmentPosition.END,
                        buttonMapping: ButtonType.UP,
                    },
                    {
                        id: 6,
                        position: SegmentPosition.BEGIN,
                        buttonMapping: ButtonType.LEFT,
                    },
                ],
            },
            {
                id: 7,
                segments: [
                    {
                        id: 6,
                        position: SegmentPosition.END,
                        buttonMapping: ButtonType.RIGHT,
                    },
                    {
                        id: 16,
                        position: SegmentPosition.BEGIN,
                        buttonMapping: ButtonType.UP,
                    },
                    {
                        id: 7,
                        position: SegmentPosition.BEGIN,
                        buttonMapping: ButtonType.LEFT,
                    },
                ],
            },
            {
                id: 8,
                segments: [
                    {
                        id: 7,
                        position: SegmentPosition.END,
                        buttonMapping: ButtonType.RIGHT,
                    },
                    {
                        id: 17,
                        position: SegmentPosition.END,
                        buttonMapping: ButtonType.UP,
                    },
                    {
                        id: 8,
                        position: SegmentPosition.BEGIN,
                        buttonMapping: ButtonType.LEFT,
                    },
                ],
            },
            {
                id: 9,
                segments: [
                    {
                        id: 9,
                        position: SegmentPosition.END,
                    },
                    {
                        id: 10,
                        position: SegmentPosition.BEGIN,
                    },
                ],
            },
            {
                id: 10,
                segments: [
                    {
                        id: 11,
                        position: SegmentPosition.BEGIN,
                    },
                    {
                        id: 10,
                        position: SegmentPosition.BEGIN,
                    },
                ],
            },
            {
                id: 11,
                segments: [
                    {
                        id: 12,
                        position: SegmentPosition.END,
                    },
                    {
                        id: 10,
                        position: SegmentPosition.BEGIN,
                    },
                ],
            },
            {
                id: 12,
                segments: [
                    {
                        id: 13,
                        position: SegmentPosition.BEGIN,
                    },
                    {
                        id: 10,
                        position: SegmentPosition.BEGIN,
                    },
                ],
            },
            {
                id: 13,
                segments: [
                    {
                        id: 14,
                        position: SegmentPosition.END,
                    },
                    {
                        id: 10,
                        position: SegmentPosition.BEGIN,
                    },
                ],
            },
            {
                id: 14,
                segments: [
                    {
                        id: 15,
                        position: SegmentPosition.BEGIN,
                    },
                    {
                        id: 10,
                        position: SegmentPosition.BEGIN,
                    },
                ],
            },
            {
                id: 15,
                segments: [
                    {
                        id: 16,
                        position: SegmentPosition.END,
                    },
                    {
                        id: 10,
                        position: SegmentPosition.BEGIN,
                    },
                ],
            },
            {
                id: 16,
                segments: [
                    {
                        id: 17,
                        position: SegmentPosition.BEGIN,
                    },
                    {
                        id: 10,
                        position: SegmentPosition.BEGIN,
                    },
                ],
            },
        ],
    },
    controllers: {
        list: [
            {
                id: 1,
                button: {
                    up: 1,
                    left: 13,
                    right: 5,
                    catch: 9,
                },
            },
            {
                id: 2,
                button: {
                    up: 2,
                    left: 14,
                    right: 6,
                    catch: 10,
                },
            },
            {
                id: 3,
                button: {
                    up: 3,
                    left: 15,
                    right: 7,
                    catch: 11,
                },
            },
            {
                id: 4,
                button: {
                    up: 4,
                    left: 16,
                    right: 8,
                    catch: 12,
                },
            },
        ],
    },
};

const mappingDev: AppConfig = {
    ...mappingProd,
    intersections: {
        list: [
            {
                id: 0,
                segments: [
                    {
                        id: 9,
                        position: SegmentPosition.END,
                    },
                    {
                        id: 14,
                        position: SegmentPosition.END,
                    },
                ],
            },
            {
                id: 1,
                segments: [
                    {
                        id: 8,
                        position: SegmentPosition.END,
                        buttonMapping: ButtonType.RIGHT,
                    },
                    {
                        id: 9,
                        position: SegmentPosition.BEGIN,
                        buttonMapping: ButtonType.UP,
                    },
                    {
                        id: 1,
                        position: SegmentPosition.BEGIN,
                        buttonMapping: ButtonType.LEFT,
                    },
                ],
            },
            {
                id: 2,
                segments: [
                    {
                        id: 1,
                        position: SegmentPosition.END,
                        buttonMapping: ButtonType.RIGHT,
                    },
                    {
                        id: 11,
                        position: SegmentPosition.END,
                        buttonMapping: ButtonType.UP,
                    },
                    {
                        id: 2,
                        position: SegmentPosition.BEGIN,
                        buttonMapping: ButtonType.LEFT,
                    },
                ],
            },
            {
                id: 3,
                segments: [
                    {
                        id: 2,
                        position: SegmentPosition.END,
                        buttonMapping: ButtonType.RIGHT,
                    },
                    {
                        id: 12,
                        position: SegmentPosition.BEGIN,
                        buttonMapping: ButtonType.UP,
                    },
                    {
                        id: 3,
                        position: SegmentPosition.BEGIN,
                        buttonMapping: ButtonType.LEFT,
                    },
                ],
            },
            {
                id: 4,
                segments: [
                    {
                        id: 3,
                        position: SegmentPosition.END,
                        buttonMapping: ButtonType.RIGHT,
                    },
                    {
                        id: 13,
                        position: SegmentPosition.END,
                        buttonMapping: ButtonType.UP,
                    },
                    {
                        id: 4,
                        position: SegmentPosition.BEGIN,
                        buttonMapping: ButtonType.LEFT,
                    },
                ],
            },
            {
                id: 5,
                segments: [
                    {
                        id: 4,
                        position: SegmentPosition.END,
                        buttonMapping: ButtonType.RIGHT,
                    },
                    {
                        id: 14,
                        position: SegmentPosition.BEGIN,
                        buttonMapping: ButtonType.UP,
                    },
                    {
                        id: 5,
                        position: SegmentPosition.BEGIN,
                        buttonMapping: ButtonType.LEFT,
                    },
                ],
            },
            {
                id: 6,
                segments: [
                    {
                        id: 5,
                        position: SegmentPosition.END,
                        buttonMapping: ButtonType.RIGHT,
                    },
                    {
                        id: 15,
                        position: SegmentPosition.END,
                        buttonMapping: ButtonType.UP,
                    },
                    {
                        id: 6,
                        position: SegmentPosition.BEGIN,
                        buttonMapping: ButtonType.LEFT,
                    },
                ],
            },
            {
                id: 7,
                segments: [
                    {
                        id: 6,
                        position: SegmentPosition.END,
                        buttonMapping: ButtonType.RIGHT,
                    },
                    {
                        id: 16,
                        position: SegmentPosition.BEGIN,
                        buttonMapping: ButtonType.UP,
                    },
                    {
                        id: 7,
                        position: SegmentPosition.BEGIN,
                        buttonMapping: ButtonType.LEFT,
                    },
                ],
            },
            {
                id: 8,
                segments: [
                    {
                        id: 7,
                        position: SegmentPosition.END,
                        buttonMapping: ButtonType.RIGHT,
                    },
                    {
                        id: 17,
                        position: SegmentPosition.END,
                        buttonMapping: ButtonType.UP,
                    },
                    {
                        id: 8,
                        position: SegmentPosition.BEGIN,
                        buttonMapping: ButtonType.LEFT,
                    },
                ],
            },
            {
                id: 9,
                segments: [
                    {
                        id: 9,
                        position: SegmentPosition.END,
                    },
                    {
                        id: 10,
                        position: SegmentPosition.BEGIN,
                    },
                ],
            },
            {
                id: 10,
                segments: [
                    {
                        id: 11,
                        position: SegmentPosition.BEGIN,
                    },
                    {
                        id: 10,
                        position: SegmentPosition.BEGIN,
                    },
                ],
            },
            {
                id: 11,
                segments: [
                    {
                        id: 12,
                        position: SegmentPosition.END,
                    },
                    {
                        id: 10,
                        position: SegmentPosition.BEGIN,
                    },
                ],
            },
            {
                id: 12,
                segments: [
                    {
                        id: 13,
                        position: SegmentPosition.BEGIN,
                    },
                    {
                        id: 10,
                        position: SegmentPosition.BEGIN,
                    },
                ],
            },
            {
                id: 13,
                segments: [
                    {
                        id: 14,
                        position: SegmentPosition.END,
                    },
                    {
                        id: 10,
                        position: SegmentPosition.BEGIN,
                    },
                ],
            },
            {
                id: 14,
                segments: [
                    {
                        id: 15,
                        position: SegmentPosition.BEGIN,
                    },
                    {
                        id: 10,
                        position: SegmentPosition.BEGIN,
                    },
                ],
            },
            {
                id: 15,
                segments: [
                    {
                        id: 16,
                        position: SegmentPosition.END,
                    },
                    {
                        id: 10,
                        position: SegmentPosition.BEGIN,
                    },
                ],
            },
            {
                id: 16,
                segments: [
                    {
                        id: 17,
                        position: SegmentPosition.BEGIN,
                    },
                    {
                        id: 10,
                        position: SegmentPosition.BEGIN,
                    },
                ],
            },
        ],
    },
};

const mappingTest: AppConfig = {
    ...mappingProd,
    controllers: {
        list: [
            {
                id: 1,
                button: {
                    up: 1,
                    left: 13,
                    right: 5,
                    catch: 9,
                },
            },
        ],
    },
};

export { mappingProd, mappingDev, mappingTest };
