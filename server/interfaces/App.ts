import { SerialPortOpenOptions } from 'serialport';
import { AutoDetectTypes } from '@serialport/bindings-cpp';

import { BoardConfig } from './Board';
import { SegmentConfig } from './Segment';
import { ControllerConfig } from './Controller';
import { ButtonMatrixMappingConfig } from './ButtonMatrix';
import { IntersectionConfig } from './Intersection';

export interface AppConfig {
    emulated: boolean;
    board: BoardConfig;
    buttonMatrix: ButtonMatrixMappingConfig;
    segments: SegmentConfig;
    intersections: IntersectionConfig;
    controllers: ControllerConfig;
    serialPort: SerialPortOpenOptions<AutoDetectTypes>;
}
