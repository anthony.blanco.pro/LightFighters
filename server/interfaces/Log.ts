export interface Log {
    level: string;
    message: string;
    timestamp: string;
}
