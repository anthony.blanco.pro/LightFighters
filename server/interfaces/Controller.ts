import { ButtonConfig } from './Button';

export interface ControllerConfig {
    list: Array<{
        id: number;
        button: ButtonConfig;
    }>;
}

export type Controller = {
    id: number;
    button: ButtonConfig;
};
