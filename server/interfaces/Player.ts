import { ButtonState } from '../enums';

export interface PlayerButton {
    id: number;
    state: ButtonState;
}
