export interface BoardConfig {
    leds: number;
    gpio: number;
    stripType: string;
    dma: number;
    brightness: number;
}
