import { Game } from '../engine';
import { BallDirection, GameDifficulty, GameType } from '../enums';

export interface GameModeExec {
    callback: () => void;
}

export interface GameModePlayer {
    id: number;
    name: string;
    groupId: number;
    intersectionId: number;
    controllerId: number;
    model: Uint32Array;
}

export interface GameModeGroup {
    id: number;
    name?: string;
}

export interface GameModeBall {
    id: number;
    groupId?: number;
    segmentId: number;
    model: Uint32Array;
    initialSpeed: number;
    direction: BallDirection;
    offsetPosition?: number;
}

export interface GameMode {
    config: GameModeConfig;
    main: GameModeMain;
}

export type GameModeMain = (g: Game, gm?: GameMode) => void;

export interface GameModeConfig {
    name: string;
    description: string;
    author: string;
    type: GameType;
    minPlayer: number;
    maxPlayer: number;
    ballRedirection: boolean;
    groupAllowed: boolean;
    defaultDifficulty: GameDifficulty;
    imageUrl: string;
    maxDuration: number;
    durationFactor: number;
    maxScore: number;
}

export interface GameModeInterface {
    id: number;
    gameMode: GameMode;
}
