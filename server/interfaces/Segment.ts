import { Segment } from '../entity';
import { ButtonType, SegmentPosition } from '../enums';

export interface SegmentConfig {
    list: Array<{
        id: number;
        indices: Array<number>;
        beginIntersection: number;
        endIntersection: number;
    }>;
}

export interface SegmentNeighbor {
    reference: Segment;
    direction: SegmentPosition;
    buttonType?: ButtonType;
}
