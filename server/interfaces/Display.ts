import { DisplayLayout } from '../enums';

export interface DisplayRotationProperties {
    priority: number;
    display: DisplayLayout;
    duration: number;
}
