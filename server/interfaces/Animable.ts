export interface Animable {
    animate(models: Uint32Array, timing: Array<number>): void;
    isAnimationsInProgress(): boolean;
}
