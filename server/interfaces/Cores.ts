import { SerialPort } from 'serialport';
import ButtonMatrix from '../core/ButtonMatrix';
import Board from '../core/Board';

import { Controller } from '.';
import { Segment, Intersection } from '../entity';

export interface Cores {
    board: Board;
    segments: Array<Segment>;
    intersections: Array<Intersection>;
    controllers: Array<Controller>;
    buttonMatrix: ButtonMatrix;
    serialPort: SerialPort;
}
