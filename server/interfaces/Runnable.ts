import { RunnableStatus } from '../enums';

const DEFAULT_REFRESH_RATE = 0;

export abstract class Runnable {
    private readonly refreshRate: number;

    protected active: boolean;
    protected status: RunnableStatus;

    private intervalId: NodeJS.Timer | undefined;
    private extraIntervalsId!: Array<NodeJS.Timer>;

    private startTime: number;
    private pauseTime: number;
    private stopTime: number | undefined;

    protected constructor(refreshRate = DEFAULT_REFRESH_RATE, autoInit = true) {
        this.refreshRate = refreshRate;
        this.active = true;
        this.status = RunnableStatus.NOT_READY;
        this.startTime = Date.now();
        this.pauseTime = 0;
        this.extraIntervalsId = [];

        if (autoInit) this.initialize();
    }

    protected abstract loop(): void;

    public initialize(): void {
        this.status = RunnableStatus.READY;
    }

    public start(): void {
        if (!this.active || this.intervalId !== undefined) return;

        this.startTime = Date.now();
        this.stopTime = undefined;
        this.status = RunnableStatus.IN_PROGRESS;
        this.intervalId = setInterval(() => {
            if (this.status === RunnableStatus.PAUSED) return;
            this.loop();
        }, this.refreshRate);
    }

    public newLoop(extraLoop: () => void, ms = 0): number {
        return this.extraIntervalsId.push(
            setInterval(() => {
                if (this.status === RunnableStatus.PAUSED) return;
                extraLoop();
            }, ms),
        );
    }

    public stop(): void {
        this.stopTime = Date.now();
        this.status = RunnableStatus.STOPPED;
        if (this.intervalId !== undefined) clearInterval(this.intervalId);
        this.intervalId = undefined;
    }

    public stopExtraLoop(): void {
        this.extraIntervalsId.forEach((intervalId) => clearInterval(intervalId));
        this.extraIntervalsId = [];
    }

    public pause(): void {
        this.stopTime = Date.now();
        this.status = RunnableStatus.PAUSED;
    }

    public resume(): void {
        if (this.stopTime !== undefined) this.pauseTime += Date.now() - this.stopTime;
        this.stopTime = undefined;
        this.status = RunnableStatus.IN_PROGRESS;
    }

    public resetTiming(): void {
        this.startTime = Date.now();
        this.stopTime = undefined;
    }

    public getRunningTimeMs(): number {
        const time = this.stopTime !== undefined ? this.stopTime : Date.now();
        return time - this.startTime - this.pauseTime;
    }

    public isStarted(): boolean {
        return this.intervalId !== undefined;
    }

    public isPaused(): boolean {
        return this.status === RunnableStatus.PAUSED;
    }

    public isInitialized(): boolean {
        return this.status === RunnableStatus.READY;
    }

    public isSleeping(): boolean {
        return this.status === RunnableStatus.SLEEP;
    }
}
