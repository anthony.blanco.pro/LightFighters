import { Cores } from './Cores';
import { ScoreBoard } from '../engine';
import { GameDifficulty } from '../enums';

export interface GameCores extends Cores {
    scoreboard?: ScoreBoard;
}

export interface GameMetaData {
    time: number;
    duration: number;
    type: number;
    status: number;
    count: {
        ball: number;
        alivePlayer: number;
        eliminatedPlayer: number;
    };
    players: Array<{
        id: number;
        name: string;
        rank: number;
        group: number;
        score: number;
    }>;
}

export interface GameRequest {
    difficulty: GameDifficulty;
    playerCount: number;
}
