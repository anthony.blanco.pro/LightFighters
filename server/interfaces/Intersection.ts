import { ButtonType, SegmentPosition } from '../enums';

export type SegmentNeighborInfo = {
    id: number;
    position: SegmentPosition;
    buttonMapping?: ButtonType;
};

export interface IntersectionOptions {
    id: number;
    segments: Array<SegmentNeighborInfo>;
}

export interface IntersectionConfig {
    list: Array<IntersectionOptions>;
}
