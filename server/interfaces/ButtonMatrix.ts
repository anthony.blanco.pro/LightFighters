export interface ButtonMatrixGpioPin {
    x: Array<number>;
    y: Array<number>;
}

export interface ButtonMatrixMappingConfig {
    gpioPins: ButtonMatrixGpioPin;
}
