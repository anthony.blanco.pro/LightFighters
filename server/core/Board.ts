import ws281x from 'rpi-ws281x';
import { BoardConfig } from '../interfaces';

/**
 * WS281X led strip board
 *      - If emulated, display all led color as array in console
 */
export default class Board {
    private pixels: Uint32Array;

    constructor(config: BoardConfig) {
        this.pixels = new Uint32Array(config.leds);
        ws281x.configure(config);
    }

    public flush(): void {
        ws281x.render(this.pixels);
    }

    public clear(pixels: number[] = []): void {
        if (pixels.length === 0) {
            this.pixels.fill(0x000000);
        } else {
            pixels.forEach((pixelId) => {
                this.pixels[pixelId] = 0x000000;
            });
        }
    }

    public reset(): void {
        this.clear();
        ws281x.reset();
    }

    public getPixels(): Uint32Array {
        return this.pixels;
    }

    public setPixel(index: number, color: number): void {
        if (index < 0 || index > this.pixels.length) return;
        this.pixels[index] = color;
    }

    public setPixelMap(map: Map<number, number>): void {
        map.forEach((value, key) => {
            this.setPixel(key, value);
        });
    }

    public setPixelArray(pixels: Uint32Array): void {
        this.pixels = pixels;
    }
}
