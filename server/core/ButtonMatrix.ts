import { Gpio } from 'onoff';
import { ButtonMatrixGpioPin, ButtonMatrixMappingConfig } from '../interfaces';

/**
 * Read button matrix
 *      - If emulated, buttons have 1 chance in 5 to simulate a pression'
 */
export default class ButtonMatrix {
    private pinGpioX: Array<Gpio> | undefined; // Write gpio pins
    private pinGpioY: Array<Gpio> | undefined; // Read gpio pins

    private pinConfig: ButtonMatrixGpioPin;
    private readonly emulated: boolean;

    private pushedButtons: Array<number>;

    constructor(config: ButtonMatrixMappingConfig, emulated = false) {
        this.pushedButtons = [];

        if (!emulated) {
            this.pinGpioX = config.gpioPins.x.map((pinID) => new Gpio(pinID, 'out'));
            this.pinGpioY = config.gpioPins.y.map((pinID) => new Gpio(pinID, 'in', 'both'));
        }

        this.pinConfig = config.gpioPins;
        this.emulated = emulated;
    }

    public read(): void {
        this.pushedButtons = [];

        let buttonID = 1;

        if (this.emulated) {
            this.pinConfig.x.forEach(() => {
                this.pinConfig.y.forEach(() => {
                    if (Math.floor(Math.random() * 4) === 0) this.pushedButtons.push(buttonID);
                    buttonID++;
                });
            });
            return;
        } else {
            this.pinGpioX?.forEach((colomn) => {
                colomn.writeSync(1);
                this.pinGpioY?.forEach((line) => {
                    if (line.readSync() === 1) {
                        this.pushedButtons.push(buttonID);
                    }
                    buttonID++;
                });
                colomn.writeSync(0);
            });
        }
    }

    public isButtonPressed(buttonID: number): boolean {
        return this.pushedButtons.indexOf(buttonID) !== -1;
    }

    public getPushedButton(): Array<number> {
        return this.pushedButtons;
    }
}
