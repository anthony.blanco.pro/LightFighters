import { Logger } from './service/Logger';

import CoresService from './service/Cores';
import ApiService from './service/Api';

const LOGGER = Logger.getInstance();

async function main(): Promise<void> {
    LOGGER.info('⚡ Starting backend services');
    await ApiService.initExpress()
        .then((listeningHosts: string) => {
            LOGGER.info(`\t- Service is running on ${listeningHosts}`);
        })
        .catch((err: Error) => {
            LOGGER.error(`\t- Backend services initialisation failed : ${err.message}`);
            process.exit(1);
        });

    LOGGER.info('⚡ Starting cores initialization');
    await CoresService.initCores()
        .then(() => LOGGER.info(`\t- Cores initialized`))
        .catch((err: Error) => {
            LOGGER.error(`\t- Cores initialisation failed : ${err.message}`);
            process.exit(2);
        });
}

main().then(() => {
    LOGGER.info('⚡ All services are ready');
});
