import express from 'express';
import { constants as HttpStatus } from 'http2';

import GameService from '../service/Game';
import { errorHandler } from '../middleware/Error';

// Base Url : /api/v1/games

const gameRouter = express.Router();

gameRouter.get('/', (req, res) => {
    const gameModeList = GameService.getGameModes().map(({ id, gameMode }) => {
        return { id, ...gameMode.config };
    });
    res.status(HttpStatus.HTTP_STATUS_OK).json(gameModeList);
});

gameRouter.post('/:id', async (req, res) => {
    const gameId = Number(req.params.id);

    try {
        await GameService.createGame(gameId);
        await GameService.initGame(req.body);
        res.sendStatus(HttpStatus.HTTP_STATUS_CREATED);
    } catch (err) {
        errorHandler(err as Error, req, res);
    }
});

gameRouter.get('/sleep', async (req, res) => {
    try {
        await GameService.sleep();
        res.sendStatus(HttpStatus.HTTP_STATUS_OK);
    } catch (err) {
        errorHandler(err as Error, req, res);
    }
});

gameRouter.get('/metadata', (req, res) => {
    try {
        const metadata = GameService.getMetadata();
        res.status(HttpStatus.HTTP_STATUS_OK).send(metadata);
    } catch (err) {
        errorHandler(err as Error, req, res);
    }
});

gameRouter.get('/start', async (req, res) => {
    try {
        await GameService.start();
        res.sendStatus(HttpStatus.HTTP_STATUS_OK);
    } catch (err) {
        errorHandler(err as Error, req, res);
    }
});

gameRouter.get('/stop', async (req, res) => {
    try {
        await GameService.stop();
        res.sendStatus(HttpStatus.HTTP_STATUS_OK);
    } catch (err) {
        errorHandler(err as Error, req, res);
    }
});

gameRouter.get('/pause', (req, res) => {
    try {
        GameService.pause();
        res.sendStatus(HttpStatus.HTTP_STATUS_OK);
    } catch (err) {
        errorHandler(err as Error, req, res);
    }
});

gameRouter.get('/resume', (req, res) => {
    try {
        GameService.resume();
        res.sendStatus(HttpStatus.HTTP_STATUS_OK);
    } catch (err) {
        errorHandler(err as Error, req, res);
    }
});

gameRouter.get('/restart', async (req, res) => {
    try {
        await GameService.prepareRestart();
        res.sendStatus(HttpStatus.HTTP_STATUS_OK);
    } catch (err) {
        errorHandler(err as Error, req, res);
    }
});

gameRouter.get('/clean', async (req, res) => {
    try {
        await GameService.clean();
        res.sendStatus(HttpStatus.HTTP_STATUS_OK);
    } catch (err) {
        errorHandler(err as Error, req, res);
    }
});

export default gameRouter;
