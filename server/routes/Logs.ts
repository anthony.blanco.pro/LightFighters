import express from 'express';
import { constants as HttpStatus } from 'http2';

import { Logger } from '../service/Logger';
import { errorHandler } from '../middleware/Error';

const logRouter = express.Router();

logRouter.get('/', (req, res) => {
    try {
        res.status(HttpStatus.HTTP_STATUS_OK).json({ logs: Logger.getLogs() });
    } catch (err) {
        errorHandler(err as Error, req, res);
    }
});

logRouter.get('/clean', (req, res) => {
    try {
        Logger.cleanLogs();
        res.sendStatus(HttpStatus.HTTP_STATUS_OK);
    } catch (err) {
        errorHandler(err as Error, req, res);
    }
});

export default logRouter;
