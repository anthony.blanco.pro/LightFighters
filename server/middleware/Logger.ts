import express from 'express';
import { Logger } from '../service/Logger';

const LOGGER = Logger.getInstance();

export function loggerHandler(
    req: express.Request,
    _res: express.Response,
    next: express.NextFunction,
) {
    LOGGER.debug(`${req.method} ${req.url} - Body : ${req.body}`);
    next();
}
