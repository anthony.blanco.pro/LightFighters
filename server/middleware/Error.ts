import express from 'express';
import { constants as HttpStatus } from 'http2';
import { Logger } from '../service/Logger';

const LOGGER = Logger.getInstance();

export function notFoundHandler(_req: express.Request, res: express.Response) {
    res.sendStatus(HttpStatus.HTTP_STATUS_NOT_FOUND);
}

export function errorHandler(err: Error, _req: express.Request, res: express.Response) {
    LOGGER.error(`\t-> ${err.message}`);
    res.status(HttpStatus.HTTP_STATUS_INTERNAL_SERVER_ERROR);
    res.json({ err: err.message });
}
