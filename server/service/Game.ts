import games from '../games';
import { Game } from '../engine';
import CoresService from './Cores';
import { GameMode, GameMetaData, GameRequest } from '../interfaces';
import { GameDifficulty, GameStoppedReason } from '../enums';
import EventEmitter from 'events';

export default class GameService {
    private static game: Game | undefined;
    private static gameModeId: number;
    private static gameRequest: GameRequest;
    private static eventEmitter: EventEmitter | undefined;

    public static async createGame(gameModeId: number): Promise<Game> {
        if (this.game !== undefined) throw Error('Game already created');

        this.eventEmitter = new EventEmitter();
        this.gameModeId = gameModeId;

        const cores = await CoresService.getCores();
        const gameMode = await GameService.getGameMode(gameModeId);
        if (gameMode === undefined) throw new Error('GameMode ID not found');
        this.game = await new Game(gameMode, cores, this.eventEmitter);

        return this.game;
    }

    // eslint-disable-next-line @typescript-eslint/no-explicit-any
    public static async initGame(body: any): Promise<void> {
        if (this.game === undefined) throw new Error('Game not created');

        const gameRequest: Partial<GameRequest> = {};

        if (body.difficulty === undefined)
            throw new Error(`Missing 'difficulty' property in request body`);

        switch (body.difficulty) {
            case 'FACILE':
                gameRequest.difficulty = GameDifficulty.EASY;
                break;
            case 'NORMAL':
                gameRequest.difficulty = GameDifficulty.NORMAL;
                break;
            case 'DIFFICILE':
                gameRequest.difficulty = GameDifficulty.HARD;
                break;
            default:
                throw new Error(`Invalid difficulty '${body.difficulty}'`);
        }

        if (body.playerCount === undefined)
            throw new Error(`Missing 'playerCount' property in request body`);

        const { minPlayer, maxPlayer } = this.game.getMode().config;
        if (body.playerCount < minPlayer || body.playerCount > maxPlayer) {
            throw new Error(
                `Invalid playerCount '${body.playerCount}' (min: ${minPlayer} and max: ${maxPlayer})`,
            );
        }

        gameRequest.playerCount = body.playerCount;

        this.gameRequest = gameRequest as GameRequest;
        await this.game.init(gameRequest as GameRequest);
    }

    public static async start(): Promise<void> {
        if (this.game === undefined) throw Error('Game not created');
        if (this.game.isStarted()) throw Error('Game already started');
        if (!this.game.isInitialized()) await this.game.init(this.gameRequest);
        this.game.start();
    }

    public static async stop(): Promise<void> {
        if (this.game === undefined) throw Error('Game not created');
        if (!this.game.isStarted()) throw Error('Game not started');
        await this.game.stop(GameStoppedReason.API_STOP_CALL);
    }

    public static pause(): void {
        if (this.game === undefined) throw Error('Game not created');
        if (!this.game.isStarted()) throw Error('Game not started');
        this.game.pause();
    }

    public static resume(): void {
        if (this.game === undefined) throw Error('Game not created');
        if (!this.game.isPaused()) throw Error('Game not paused');
        this.game.resume();
    }

    public static async prepareRestart(): Promise<void> {
        if (this.game === undefined) throw Error('Game not created');
        await this.clean();
        await this.createGame(this.gameModeId);
        await this.game.init(this.gameRequest);
    }

    public static async sleep(): Promise<void> {
        if (this.game !== undefined && this.game.isSleeping()) {
            await this.game.stop(GameStoppedReason.API_SLEEP_CALL);
            await this.game.reset();
            delete this.game;
        } else {
            if (this.game !== undefined) {
                await this.game.reset();
                delete this.game;
            }
            await (await GameService.createGame(0)).sleep();
        }
    }

    static async clean(): Promise<void> {
        if (this.game === undefined) {
            console.log('No game');
            return;
        }
        if (this.game.isStarted()) await this.game.stop(GameStoppedReason.API_CLEAN_CALL);
        this.game.clean().then(() => {
            delete this.eventEmitter;
            delete this.game;
        });
    }

    public static getMetadata(): GameMetaData {
        if (GameService.game === undefined) throw Error('Game not created');
        return GameService.game.getMetaData();
    }

    public static getGameMode(id: number): GameMode {
        return games.filter((_, idx) => idx === id).map((gameMode) => gameMode)[0];
    }

    public static getGameModes(): Array<{ id: number; gameMode: GameMode }> {
        return games.map((gameMode, idx) => {
            return {
                id: idx,
                gameMode: gameMode,
            };
        });
    }
}
