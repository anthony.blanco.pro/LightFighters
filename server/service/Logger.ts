import winston from 'winston';

import { getDateFormatted } from '../utils/utils';
import { LogLevel } from '../enums/Logger';
import { Log } from '../interfaces';

const DEFAULT_LOG_LEVEL = LogLevel.info;
const DEFAULT_LOG_BUFFER_COUNT = 50;

const format = winston.format.combine(
    winston.format.timestamp({ format: 'YYYY-MM-DD HH:mm:ss' }),
    winston.format.colorize({ all: true }),
    winston.format.printf((info) => `${info.timestamp} ${info.level}: ${info.message}`),
);

const transports = [
    new winston.transports.Console({
        format,
        level: LogLevel.debug,
    }),
    new winston.transports.File({
        format,
        level: DEFAULT_LOG_LEVEL,
        dirname: 'logs',
        filename: `${getDateFormatted()}.log`,
    }),
];

export class Logger {
    private static logBuffer: Array<Log>;
    private static instance: winston.Logger;

    public static getInstance(): winston.Logger {
        if (this.instance === undefined) {
            this.logBuffer = [];
            this.instance = winston.createLogger({
                transports,
            });

            this.instance.on('data', (data) => {
                this.logBuffer.push({
                    timestamp: new Date().toString(),
                    message: data.message,
                    level: data.level,
                });
                if (this.logBuffer.length > DEFAULT_LOG_BUFFER_COUNT) {
                    this.logBuffer.shift();
                }
            });
        }

        return this.instance;
    }

    public static getLogs(): Array<Log> {
        return this.logBuffer;
    }

    static cleanLogs(): void {
        this.logBuffer = [];
    }
}
