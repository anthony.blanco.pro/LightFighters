import cors from 'cors';
import express, { Router } from 'express';

import gameRouter from '../routes/Game';
import logRouter from '../routes/Logs';

import { loggerHandler } from '../middleware/Logger';
import { notFoundHandler } from '../middleware/Error';
import { getLocalIpAddresses } from '../utils/utils';

const PORT = 3000;
const HOSTS = getLocalIpAddresses();
const BASE_URL = '/api/v1';
const STATIC_FILES = 'build';

export default abstract class ApiService {
    public static initExpress(): Promise<string> {
        return new Promise((resolve, reject) => {
            try {
                const app = express();
                const api = Router();
                const listeningHostStr = `[${HOSTS.join(', ')}]:${PORT}`;

                app.use(express.json());
                app.use(cors());
                app.use(loggerHandler);
                app.use(express.static(STATIC_FILES));
                app.use(`${BASE_URL}`, api);
                api.use('/games', gameRouter);
                api.use('/logs', logRouter);
                app.use('*', notFoundHandler);

                app.listen(PORT, () => resolve(listeningHostStr));
            } catch (err) {
                reject(err);
            }
        });
    }
}
