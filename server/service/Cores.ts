import { Cores } from '../engine';
import { Cores as CoresObject } from '../interfaces';

import { mappingProd } from '../mappings/mapping';

const APP_MAPPING = mappingProd;

export default class CoresService {
    private static cores: CoresObject | undefined;

    public static async initCores(): Promise<void> {
        CoresService.cores = await new Cores().getCores(APP_MAPPING);
    }

    public static clearCores(): void {
        CoresService.cores = undefined;
    }

    public static getCores(): CoresObject {
        if (CoresService.cores === undefined) throw new Error('Cores is not initialized');
        return CoresService.cores;
    }
}
