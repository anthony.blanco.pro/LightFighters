import os from 'os';

export function sleep(ms: number): Promise<void> {
    return new Promise((resolve) => setTimeout(resolve, ms));
}

export function validate<T>(object: T | undefined, error?: Error): T {
    if (object === undefined) throw error ?? new Error(`Object is undefined`);
    return object;
}

export function centerString(
    value: string,
    desiredSize: number,
    char = ' ',
    prefix = '',
    suffix = '',
): string {
    let formattedValueStr = prefix;
    if (value.length >= desiredSize) {
        formattedValueStr = `${formattedValueStr}${value}`;
    } else {
        const nbSpaceToAdd = (desiredSize - value.length) / 2;
        const prefixLength = Math.ceil(nbSpaceToAdd);
        const suffixLength = Math.floor(nbSpaceToAdd);

        formattedValueStr = `${Array(prefixLength).fill(char).join('')}${value}${Array(suffixLength)
            .fill(char)
            .join('')}`;
    }
    formattedValueStr = `${formattedValueStr}${suffix}`;
    return formattedValueStr;
}

export function getLocalIpAddresses(): Array<string> {
    const allowedInterface = ['eth0', 'wlan0'];
    return validate(
        Object.entries(os.networkInterfaces())
            .filter(([addresses]) => allowedInterface.includes(addresses))
            .map((data) => data[1])[0]
            ?.filter((address) => address.family === 'IPv4')
            .map((address) => address.address),
        new Error('No Network interface found (required : IPv4 network)'),
    );
}

export function getDateFormatted(): string {
    const CURRENT_DATE = new Date(Date.now());
    const CURRENT_DATE_DAY = CURRENT_DATE.getDate().toString().padStart(2, '0');
    const CURRENT_DATE_MONTH = CURRENT_DATE.getMonth().toString().padStart(2, '0');
    const CURRENT_DATE_YEAR = CURRENT_DATE.getFullYear();
    return `${CURRENT_DATE_YEAR}-${CURRENT_DATE_MONTH}-${CURRENT_DATE_DAY}`;
}
