declare module 'oled-i2c-bus' {
    export interface OledOptions {
        address: number;
        width: number;
        height: number;
    }

    export default class oled {
        constructor(i2cBus: Bus, options?: Partial<OledOptions>);

        clearDisplay(sync = true): void;
        dimDisplay(value: true | false): void;
        invertDisplay(value: true | false): void;
        turnOffDisplay(): void;
        turnOnDisplay(): void;
        drawPixel(pixels: Array<Array<number>>): void;
        drawLine(x0: number, y0: number, x1: number, y1: number, color: number): void;
        fillRect(x0: number, y0: number, width: number, height: number, color: number): void;
        startscroll(direction: 'left' | 'right', start: number, stop: number): void;
        stopscroll(): void;
        setCursor(x: number, y: number): void;
        writeString(
            font: OledFonts,
            fontSize: number,
            text: string,
            color: number,
            wrap: boolean,
            sync = true,
        ): void;
        update(): void;
    }
}
