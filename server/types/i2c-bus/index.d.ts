/* eslint-disable @typescript-eslint/no-explicit-unknown */

declare module 'i2c-bus' {
    export type I2cOptions = {
        width: number;
        height: number;
        address: number;
    };

    export class I2cFuncs {
        public i2c: boolean;
        public tenBitAddr: boolean;
        public protocolMangling: boolean;
        public smbusPec: boolean;
        public smbusBlockProcCall: boolean;
        public smbusQuick: boolean;
        public smbusReceiveByte: boolean;
        public smbusSendByte: boolean;
        public smbusReadByte: boolean;
        public smbusWriteByte: boolean;
        public smbusReadWord: boolean;
        public smbusWriteWord: boolean;
        public smbusProcCall: boolean;
        public smbusReadBlock: boolean;
        public smbusWriteBlock: boolean;
        public smbusReadI2cBlock: boolean;
        public smbusWriteI2cBlock: boolean;
    }

    export class PromisifiedBus {
        close(): Promise<void, string>;
        i2cFuncs(): Promise<I2cFuncs, string>;
        scan(startaddr: number, endaddr: number): Promise<Array<number>, string>;
        deviceId(addr: number): Promise<number, string>;
        i2cRead(addr: number, length: number, buffer: unknown): Promise<unknown, string>;
        i2cWrite(addr: number, length: number, buffer: unknown): Promise<unknown, string>;
        readByte(addr: number, cmd: number): Promise<number, string>;
        readWord(addr: number, cmd: number): Promise<number, string>;
        readI2cBlock(
            addr: number,
            cmd: number,
            length: number,
            buffer: unknown,
        ): Promise<unknown, string>;
        sendByte(addr: number, byte): Promise<void, string>;
        receiveByte(addr: number): Promise<number, string>;
        writeByte(addr: number, cmd: number, byte: number): Promise<void, string>;
        writeWord(addr: number, cmd: number, word: number): Promise<void, string>;
        writeQuick(addr: number, bit: number): Promise<void, string>;
        writeI2cBlock(
            addr: number,
            cmd: number,
            length: number,
            buffer: unknown,
        ): Promise<unknown, string>;
        bus(): Bus;
    }

    export class Bus {
        close(cb: (err: string) => void): void;
        closeSync(): void;
        i2cFuncs(cb: (cb: string, funcs) => void): void;
        i2cFuncsSync(): I2cFuncs;
        scan(
            startaddr?: number,
            endaddr?: number,
            cb?: (err: string, devices: Array<number>) => void,
        ): void;
        scanSync(startaddr?: number, endaddr?: number): Array<number>;
        deviceId(addr: number, cb: (err: string, id: unknown) => void): void;
        deviceIdSync(addr: number): unknown;
        i2cRead(
            addr: number,
            length: number,
            buffer: unknown,
            cb: (err: string, bytesRead: number, buffer: unknown) => void,
        ): void;
        i2cReadSync(addr: number, length: number, buffer: unknown): number;
        i2cWrite(
            addr: number,
            length: number,
            buffer: unknown,
            cb: (err: string, bytesWritten: number, buffer: unknown) => void,
        ): void;
        i2cWriteSync(addr: number, length: number, buffer: unknown): number;
        readByte(addr: number, cmd: number, cb: (err: string, byte: number) => void): void;
        readByteSync(addr: number, cmd: number): void;
        readWord(addr: number, cmd: number, cb: (err: string, word: number) => void): void;
        readWordSync(addr: number, cmd: number): void;
        readI2cBlock(
            addr: number,
            cmd: number,
            length: number,
            buffer: unknown,
            cb: (err: string, bytesRead: number, buffer: unknown) => void,
        ): void;
        readI2cBlockSync(addr: number, cmd: number, length: number, buffer: unknown): number;
        receiveByte(addr: number, cb: (err: string, byte: number) => void): void;
        receiveByteSync(addr: number): number;
        sendByte(addr: number, byte: number, cb: (err: string) => void): void;
        sendByteSync(addr: number, byte: number): void;
        writeByte(addr: number, cmd: number, byte: number, cb: (err: string) => void): void;
        writeByteSync(addr: number, cmd: number, byte: number): void;
        writeWord(addr: number, cmd: number, word: string, cb: (err: string) => void): void;
        writeWordSync(addr: number, cmd: number, word: string): void;
        writeQuick(addr: number, bit: number, cb: (err: string) => void): void;
        writeQuickSync(addr: number, bit: number): void;
        writeI2cBlock(
            addr: number,
            cmd: number,
            length: number,
            buffer: unknown,
            cb: (err: string, bytesWritten: number, buffer: unknown) => void,
        ): void;
        writeI2cBlockSync(addr: number, cmd: number, length: number, buffer: unknown): void;
        promisifiedBus(): PromisifiedBus;
    }

    declare function open(busNumber: number, options?: I2cOptions, cb: (err: string) => void): Bus;
    declare function openSync(busNumber: number, options?: I2cOptions): Bus;
    declare function openPromisified(
        busNumber: number,
        options?: I2cOptions,
    ): Promise<PromisifiedBus>;
}
