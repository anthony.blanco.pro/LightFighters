import { Game } from '../engine';

declare const Buffer;

declare global {
    namespace Express {
        interface Request {
            game: Game;
        }
    }
}
