export enum IntersectionAreaType {
    ACTIVE_AREA,
    PASSIVE_AREA,
}

export enum IntersectionEvents {
    ON_BALL_ENTERING_IN_AREA = 'onBallEnteringInArea',
    ON_BALL_LEAVING_AREA = 'onBallLeavingInArea',
}
