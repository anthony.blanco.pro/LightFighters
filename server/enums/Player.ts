export enum PlayerEvent {
    ON_BUTTON_PRESSED = 'onButtonPressed',
    ON_BUTTON_RELEASE = 'onButtonRelease',
}

export enum PlayerInteractions {
    PLAYER_CLICKED,
}
