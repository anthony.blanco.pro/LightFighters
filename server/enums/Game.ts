export enum GameType {
    SCORE,
    LIFE,
}

export enum GameDifficulty {
    EASY = 'EASY',
    NORMAL = 'NORMAL',
    HARD = 'HARD',
}

export enum GameStoppedReason {
    TIME_OVER,
    PLAYER_REACH_MAX_SCORE,
    ALL_PLAYERS_ARE_ELIMINATED,
    API_SLEEP_CALL,
    API_STOP_CALL,
    API_RESTART_CALL,
    API_CLEAN_CALL,
}

export enum GameProperty {
    // Scoreboard
    S_DISPLAY_ROTATION,
}

export enum GameEvents {
    BEFORE_INIT = 'beforeInit',

    ON_INIT = 'onInit',
    ON_READY = 'onReady',
    ON_START = 'onGameStart',
    ON_STOP = 'onGameStop',
    ON_RESUME = 'onGameResume',
    ON_RESTART = 'onGameRestart',
    ON_PAUSE = 'onGamePause',
    ON_TICK = 'onGameTick',

    ON_BALL_CREATED = 'onBallCreated',
    ON_BALL_MOVING = 'onBallMoving',
    ON_BALL_STOPPED = 'onBallStopped',
    ON_BALL_ENTERING_AREA = 'onBallEnteringArea',
    ON_BALL_LEAVING_AREA = 'onBallLeavingArea',
    ON_BALL_LEAVING_AREA_WITHOUT_INTERACTIONS = 'onBallLeavingAreaWithoutInteractions',

    ON_PLAYER_CREATED = 'onPlayerCreated',
    ON_PLAYER_ELIMINATED = 'onPlayerEliminated',
    ON_PLAYER_CLICK = 'onPlayerClick',
    ON_PLAYER_CLICK_RELEASE = 'onPlayerClickRelease',
    ON_PLAYER_CLICK_IN_HIS_AREA = 'onPlayerClickInHisArea',
    ON_PLAYER_CLICK_IN_PASSIVE_AREA = 'onPlayerClickInPassiveArea',
    ON_PLAYER_CLICK_IN_ACTIVE_AREA = 'onPlayerClickInActiveArea',
    ON_PLAYER_CLICK_OUTSIDE_AREAS = 'onPlayerClickOutsideAreas',
    ON_PLAYER_CLICK_MULTIPLE_TIMES_IN_AREA = 'onPlayerClickMultipleTimeInArea',
}

export enum GameAnimation {
    BLINK,
    BLINK_COLOR,
    FADE,
}
