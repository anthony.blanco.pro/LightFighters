export enum AreaDrawDirection {
    FORWARD = 1,
    BACKWARD = -1,
}
