export enum DisplayLayout {
    SCORE,
    LIFE_COUNT,
    LOADING,
    SPLASH,
    SLEEP,
    TIME_LEFT,
    RANKS,
    PERSONAL_RANK,
    CREDIT,
    PAUSE = ';',
    EASTER_EGG = ':',
}
