export enum BallDirection {
    FORWARD = 1,
    BACKWARD = -1,
}

export enum BallStatus {
    LEAVING = -1,
    IDLE = 0,
    PAUSE = 1,
}

export enum BallInteractions {
    BALL_CLICKED,
    LEAVING_AREA,
    ENTERING_AREA,
}
