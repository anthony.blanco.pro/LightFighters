export enum RunnableStatus {
    NOT_READY,
    LOADING,
    READY,
    IN_PROGRESS,
    PAUSED,
    STOPPED,
    SLEEP,
}
