export enum ButtonType {
    UP = 'UP',
    LEFT = 'LEFT',
    RIGHT = 'RIGHT',
    CATCH = 'CATCH',
    UNKNOWN = 'UNKNOWN',
}

export enum ButtonState {
    UP,
    DOWN,
}
