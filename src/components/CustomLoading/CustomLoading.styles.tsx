export const CustomLoadingStyles: any = {
    LoadingSVG: {
        position: 'relative !important',
        margin: '0 auto !important',
        width: '112px !important',
        height: '112px !important',
    },
    SubLoading: {
        color: '#0971F1 !important',
        position: 'absolute !important',
        left: '0',
    },
    Loading: {
        position: 'absolute !important',
        left: '0',
        color: '#FFFFFF !important',
    },
    LoadingText: {
        textAlign: 'center',
        marginTop: '2rem !important',
        margin: '0 auto !important',
    },
};
