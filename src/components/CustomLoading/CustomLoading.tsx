import { makeStyles } from '@mui/styles';
import { ReactElement } from 'react';
import { CustomLoadingStyles } from './CustomLoading.styles';
import { CircularProgress, Container } from '@mui/material';
import Title from '../Title/Title';

export default function CustomLoading(props: any): ReactElement {
    const classes = makeStyles(CustomLoadingStyles)();

    return (
        <div style={props.style}>
            <Container className={classes.LoadingSVG}>
                <CircularProgress
                    className={classes.SubLoading}
                    variant="determinate"
                    size={'7rem'}
                    thickness={4}
                    value={100}
                />
                <CircularProgress
                    disableShrink
                    className={classes.Loading}
                    size={'7rem'}
                    thickness={4}
                />
            </Container>

            <Container className={classes.LoadingText}>
                <Title>{props.text}</Title>
            </Container>
        </div>
    );
}
