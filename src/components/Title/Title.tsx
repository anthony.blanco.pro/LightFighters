import { ReactElement } from 'react';
import { makeStyles } from '@mui/styles';
import { TitleStyles } from './Title.styles';

export default function Title(props: any): ReactElement {
    const classes = makeStyles(TitleStyles)();
    return <h1 className={classes.Title}>{props.children}</h1>;
}

export { Title };
