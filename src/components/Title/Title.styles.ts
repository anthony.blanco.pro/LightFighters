const TitleStyles: any = {
    Title: {
        margin: '0 !important',
        paddingLeft: '0 !important',
        color: '#FFFFFF !important',
        fontFamily: `Poppins-Bold !important`,
        fontWeight: 'bold !important',
        fontSize: '30px !important',
    },
};

export { TitleStyles };
