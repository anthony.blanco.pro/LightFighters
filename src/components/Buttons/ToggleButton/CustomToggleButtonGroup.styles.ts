export const CustomToggleButtonGroupStyles: any = {
    ToggleButtonGroup: {
        width: '65vh',
        display: 'inline-block !important',
    },
    Column: {
        display: 'block',
        width: '100% !important',
        height: '4.5rem !important',
    },
    Row: {
        display: 'inline-block !important',
        width: '25vh !important',
        height: '25vh !important',
        marginLeft: '1rem !important',
        marginRight: '1rem !important',
    },
    ToggleButton: {
        marginTop: '1rem !important',
        background: '#141493 !important',
        border: '1px solid rgba(255, 255, 255, 0.51) !important',
        boxSizing: 'border-box !important',
        borderRadius: '10px !important',
    },

    SelectedToggleButton: {
        border: '3px solid #FFFFFF !important',
    },

    ToggleButtonText: {
        textTransform: 'none',
        padding: '0 !important',
        margin: '0 !important',
        color: '#FFFFFF !important',
        textAlign: 'center',
        fontFamily: `Poppins-Bold !important`,
        fontWeight: 'bold !important',
        fontSize: '30px !important',
        verticalAlign: 'middle !important',
    },
};
