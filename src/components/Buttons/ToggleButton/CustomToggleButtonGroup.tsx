import { ReactElement } from 'react';
import { makeStyles } from '@mui/styles';

import { CustomToggleButtonGroupStyles } from './CustomToggleButtonGroup.styles';
import { ToggleButton, ToggleButtonGroup } from '@mui/material';

export default function CustomToggleButtonGroup(props: any): ReactElement {
    const classes = makeStyles(CustomToggleButtonGroupStyles)();
    return (
        <ToggleButtonGroup
            className={classes.ToggleButtonGroup}
            orientation="vertical"
            value={props.value}
            exclusive
            onChange={props.onChange}
        >
            {props.items.map((item: any, index: number) => {
                let styleToggleButton = { ToggleButton: {} };

                if (props.disposition == 'row') {
                    styleToggleButton.ToggleButton = {
                        ...CustomToggleButtonGroupStyles.Row,
                        ...CustomToggleButtonGroupStyles.ToggleButton,
                    };
                } else {
                    styleToggleButton.ToggleButton = {
                        ...CustomToggleButtonGroupStyles.Column,
                        ...CustomToggleButtonGroupStyles.ToggleButton,
                    };
                }

                if (item === props.value) {
                    styleToggleButton.ToggleButton = {
                        ...styleToggleButton.ToggleButton,
                        ...CustomToggleButtonGroupStyles.SelectedToggleButton,
                    };
                }

                return (
                    <ToggleButton
                        value={props.itemLinks ? props.itemLinks[index] : item}
                        key={index}
                        className={makeStyles(styleToggleButton)().ToggleButton}
                    >
                        <p className={classes.ToggleButtonText}>{item}</p>
                    </ToggleButton>
                );
            })}
        </ToggleButtonGroup>
    );
}

export { CustomToggleButtonGroup };
