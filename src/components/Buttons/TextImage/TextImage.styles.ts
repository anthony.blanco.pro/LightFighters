export const TextImageStyles: any = {
    Text: {
        color: '#FFFFFF',
        fontSize: '18px',
        fontFamily: 'Poppins',
    },
    Image: {
        maxWidth: '65vh',
        float: 'right',
        maxHeight: '45vh',
    },
};
