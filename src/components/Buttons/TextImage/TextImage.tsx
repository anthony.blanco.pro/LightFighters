import { ReactElement } from 'react';
import { makeStyles } from '@mui/styles';

import { TextImageStyles } from './TextImage.styles';

export default function TextImage(props: any): ReactElement {
    const classes = makeStyles(TextImageStyles)();

    return (
        <p className={classes.Text}>
            {props.image === '' || props.image === undefined ? (
                ''
            ) : (
                <img src={props.image} alt="image description" className={classes.Image} />
            )}
            {props.description}
        </p>
    );
}

export { TextImage };
