export const StyledTabsStyles: any = {
    TabSpan: {
        '& .MuiTabs-indicator': {
            display: 'flex',
            justifyContent: 'center',
            backgroundColor: 'transparent',
            height: '4px',
        },
        '& .MuiTabs-indicatorSpan': {
            width: '100%',
            backgroundColor: '#47BCFE',
        },
    },
};
