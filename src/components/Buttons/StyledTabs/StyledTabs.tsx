import { TabList } from '@mui/lab';
import { styled } from '@mui/styles';
import { ReactNode, SyntheticEvent } from 'react';
import { StyledTabsStyles } from './StyledTabs.styles';

interface StyledTabsProps {
    children?: ReactNode;
    value: string;
    onChange: (event: SyntheticEvent, tab: string) => void;
}

export const StyledTabs = styled((props: StyledTabsProps) => (
    <TabList
        {...props}
        TabIndicatorProps={{ children: <span className="MuiTabs-indicatorSpan" /> }}
    />
))({
    ...StyledTabsStyles.TabSpan,
});
