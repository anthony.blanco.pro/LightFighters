const HardButtonStyles: any = {
    HardButton: {
        padding: '0.75em 3.5em !important',
        borderRadius: '56px !important',
        backgroundColor: '#47BCFE !important',
        color: '#FFFFFF !important',
        textAlign: 'center',
        fontFamily: `Poppins-Bold !important`,
        fontWeight: 'bold !important',
        fontSize: '20px !important',
    },
    HardButtonDisable: {
        color: 'gray     !important',
        padding: '0.75em 3.5em !important',
        borderRadius: '56px !important',
        backgroundColor: 'rgba(0, 0, 0, 0.26) !important',
        textAlign: 'center',
        fontFamily: `Poppins-Bold !important`,
        fontWeight: 'bold !important',
        fontSize: '20px !important',
    },
};

export { HardButtonStyles };
