import { Button } from '@mui/material';
import { makeStyles } from '@mui/styles';
import { ReactElement } from 'react';
import { HardButtonStyles } from './HardButton.styles';

export default function HardButtons(props: any): ReactElement {
    const classes = makeStyles(HardButtonStyles)();
    return (
        <Button
            className={props.disable ? classes.HardButtonDisable : classes.HardButton}
            variant={props.btnType}
            onClick={props.onClick}
            sx={props.class}
            disabled={props.disable}
        >
            {props.title}
        </Button>
    );
}
