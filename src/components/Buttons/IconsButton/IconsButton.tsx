import { Button, Icon } from '@mui/material';
import { makeStyles } from '@mui/styles';
import { ReactElement } from 'react';
import { IconsButtonStyles } from './IconsButton.styles';

export default function IconsButton(props: any): ReactElement {
    const classes = makeStyles(IconsButtonStyles)();

    return (
        <Button
            onClick={props.onClick}
            className={props.disable ? classes.IconsButtonDisable : classes.IconsButton}
            sx={props.class}
            disabled={props.disable}
        >
            <Icon component={props.icon} id={props.id} fontSize="inherit" sx={props.class} />
        </Button>
    );
}
