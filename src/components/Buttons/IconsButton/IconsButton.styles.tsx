const IconsButtonStyles = {
    IconsButton: {
        color: '#FFFFFF !important',
        fontWeight: 'bold !important',
        fontSize: '4.5rem !important',
    },
    IconsButtonDisable: {
        color: 'rgba(0, 0, 0, 0.26) !important',
        fontWeight: 'bold !important',
        fontSize: '4.5rem !important',
    },
};

export { IconsButtonStyles };
