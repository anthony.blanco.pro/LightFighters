import { Icon } from '@mui/material';
import { ReactElement } from 'react';

export default function Icons(props: any): ReactElement {
    return <Icon component={props.icon} id={props.id} className={props.class} />;
}
