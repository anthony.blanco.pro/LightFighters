import { makeStyles } from '@mui/styles';
import { ReactElement } from 'react';
import { ImageStyles } from './Image.styles';

export default function Image({ src, alt }: any): ReactElement {
    const classes = makeStyles(ImageStyles)();
    return (
        <div className={classes.ContainerImage}>
            <img className={classes.Image} src={src} alt={alt} />
        </div>
    );
}
