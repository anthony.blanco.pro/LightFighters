import { Slide, Dialog, DialogContent } from '@mui/material';
import { makeStyles, styled } from '@mui/styles';
import { ReactElement } from 'react';
import { CustomDialogStyles } from './CustomDialog.styles';
import React from 'react';
import { TransitionProps } from '@mui/material/transitions';
import HardButtons from '../Buttons/HardButton/HardButton';

export default function CustomDialog(props: any): ReactElement {
    const classes = makeStyles(CustomDialogStyles)();

    const Transition = React.forwardRef(function Transition(
        props: TransitionProps & {
            children: React.ReactElement<any, any>;
        },
        ref: React.Ref<unknown>,
    ) {
        return <Slide direction="up" ref={ref} {...props} />;
    });

    const StyledDialog = styled(Dialog)(() => ({
        '& .MuiDialog-paper': {
            ...CustomDialogStyles.PopUpPause,
        },
    }));

    return (
        <StyledDialog
            open={props.dialogState}
            sx={CustomDialogStyles.PopUpPause}
            className={classes.PopUpPause}
            TransitionComponent={Transition}
            keepMounted
            onClose={props.handleResume}
        >
            <DialogContent className={classes.PopUpPauseContent}>
                <HardButtons
                    title="Reprendre"
                    onClick={props.handleResume}
                    class={CustomDialogStyles.PopUpPauseButton}
                />
                <HardButtons
                    title="Rejouer"
                    onClick={props.handleRestart}
                    class={CustomDialogStyles.PopUpPauseButton}
                />
                <HardButtons
                    title="Quitter"
                    onClick={props.handleExit}
                    class={CustomDialogStyles.PopUpPauseButton}
                />
            </DialogContent>
        </StyledDialog>
    );
}
