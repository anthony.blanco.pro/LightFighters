export const CustomDialogStyles: any = {
    PopUpPause: {
        background: 'transparent !important',
        boxShadow: 'none',
    },
    PopUpPauseButton: {
        display: 'block',
        marginTop: '1rem',
        marginBottom: '1rem',
        width: '100%',
    },
};
