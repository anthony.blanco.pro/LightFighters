import { TableContainer, TableHead, TableRow, TableCell, TableBody, Table } from '@mui/material';

import { makeStyles } from '@mui/styles';
import { ReactElement } from 'react';
import { Player } from '../../interfaces/Player';
import { CustomTableStyles } from './CustomTable.styles';

export default function CustomTable(props: any): ReactElement {
    const classes = makeStyles(CustomTableStyles)();

    const sortedData = props.data.sort((current: any, next: any) => {
        return current.rank - next.rank;
    });

    return (
        <TableContainer>
            <Table>
                <TableHead>
                    <TableRow>
                        {props.heads.map((head: string) => (
                            <TableCell className={classes.TableCellHead} key={head}>
                                {head}
                            </TableCell>
                        ))}
                    </TableRow>
                </TableHead>
                <TableBody>
                    {sortedData.map((element: Player) => (
                        <TableRow key={element.id}>
                            <TableCell className={classes.TableCellBody}>{element.rank}</TableCell>
                            <TableCell className={classes.TableCellBody}>{element.name}</TableCell>
                            <TableCell className={classes.TableCellBody}>{element.score}</TableCell>
                        </TableRow>
                    ))}
                </TableBody>
            </Table>
        </TableContainer>
    );
}
