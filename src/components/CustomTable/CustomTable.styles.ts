export const CustomTableStyles: any = {
    TableCellHead: {
        padding: '12px 5px !important',
        textAlign: 'center !important',
        color: '#FFFFFF !important',
        fontFamily: 'Poppins-Bold !important',
        fontWeight: 'bold !important',
        fontSize: '25px !important',
        border: '0 !important',
    },
    TableCellBody: {
        padding: '12px !important',
        textAlign: 'center !important',
        color: '#FFFFFF !important',
        fontFamily: 'Poppins-Bold !important',
        fontWeight: 'bold !important',
        fontSize: '20px !important',
        border: '0 !important',
    },
};
