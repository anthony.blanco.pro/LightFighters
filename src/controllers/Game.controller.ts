import { GameMode, Metadata } from '../interfaces/Game';

const baseUrl = 'http://127.0.0.1:3000/api/v1/games/';

export async function getModes(): Promise<GameMode[]> {
    return fetch(baseUrl)
        .then((response) => {
            return response.json();
        })
        .then((gameModes: any) => {
            return gameModes as GameMode[];
        })
        .catch(() => {
            return [] as GameMode[];
        });
}

export function getNameModes(gameModes: Partial<GameMode[]>): string[] {
    return gameModes.map((gameMode) => (gameMode === undefined ? '' : gameMode.name));
}

export function getInformations(gameModes: GameMode[], nameMode: String): GameMode {
    const gameMode =
        gameModes.find((gameMode: GameMode) => gameMode.name === nameMode) || gameModes[0];
    gameMode.defaultDifficulty =
        gameMode.defaultDifficulty.charAt(0) + gameMode.defaultDifficulty.slice(1).toLowerCase();

    return gameMode;
}

export async function setGame(
    id: number,
    difficulty: string,
    playerCount: number,
): Promise<Response> {
    const options = {
        method: 'POST',
        headers: { 'Content-Type': 'application/json' },
        body: JSON.stringify({ difficulty: difficulty.toUpperCase(), playerCount: playerCount }),
    };
    return fetch(baseUrl + id, options)
        .then((response) => {
            return response;
        })
        .catch((error) => {
            return error;
        });
}

export async function updateGame(status: string): Promise<Response> {
    let url = baseUrl + status;
    return fetch(url)
        .then((response) => {
            return response;
        })
        .catch((error) => {
            return error;
        });
}

export async function getMetadata(): Promise<Metadata> {
    return fetch(baseUrl + 'metadata')
        .then((response) => {
            return response.json();
        })
        .then((data) => {
            return data as Metadata;
        })
        .catch(() => {
            return {} as Metadata;
        });
}
