import { ReactElement } from 'react';

import Router from './Route';
import AppLayout from './layout/AppLayout/AppLayout';

export default function App(): ReactElement {
    return (
        <AppLayout>
            <Router />
        </AppLayout>
    );
}
