export enum GameStatus {
    NOT_READY = 0,
    LOADING = 1,
    READY = 2,
    IN_PROGRESS = 3,
    PAUSED = 4,
    STOPPED = 5,
    SLEEP = 6,
}

export enum GameType {
    SCORE = 0,
    LIFE = 1,
}

export enum Difficulty {
    EASY = 0,
    NORMAL = 1,
    HARD = 2,
}
