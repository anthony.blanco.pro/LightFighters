const LoadingStyles: any = {
    Content: {},
    ContentDisable: {
        display: 'none !important',
    },
    LoadingContent: {
        marginTop: '33vh !important',
    },

    ReadyContent: {
        textAlign: 'center',
        marginTop: '33vh !important',
    },
    ReadyTile: {
        fontSize: '25px !important',
    },
    ReadyCount: {
        textAlign: 'center',
        marginTop: '1rem !important',
        paddingLeft: '0 !important',
        color: '#FFFFFF !important',
        fontFamily: `Poppins-Bold !important`,
        fontWeight: 'bold !important',
        fontSize: '100px !important',
    },
};

export { LoadingStyles };
