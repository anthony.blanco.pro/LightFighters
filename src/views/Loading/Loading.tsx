import { ReactElement, useEffect, useState } from 'react';
import { useNavigate } from 'react-router-dom';
import { makeStyles } from '@mui/styles';

import { LoadingStyles } from './Loading.styles';
import { Container } from '@mui/material';
import { Title } from '../../components/Title/Title';
import { getMetadata, updateGame } from '../../controllers/Game.controller';
import { GameStatus } from '../../enums/Game';
import CustomLoading from '../../components/CustomLoading/CustomLoading';
import { Metadata } from '../../interfaces/Game';

export default function Loading(): ReactElement {
    const classes = makeStyles(LoadingStyles)();
    const navigate = useNavigate();

    const [delay, setDelay] = useState(-1);
    const [status, setStatus] = useState(1);
    let intervalId: NodeJS.Timer;

    useEffect(() => {
        intervalId = setInterval(() => {
            getMetadata().then((metadata: Metadata) => {
                if (metadata.status == undefined) navigate('/error');
                else {
                    setStatus(metadata.status);
                    if (metadata.status === GameStatus.READY) {
                        clearInterval(intervalId);
                        setDelay(3);
                    }
                }
            });
        }, 1000);
    }, []);

    useEffect(() => {
        setTimeout(() => {
            if (delay > 0) setDelay(delay - 1);

            if (delay === 0) {
                updateGame('start').then((response) => {
                    if (response.status == 200) navigate('/ingame');
                    else navigate('/error');
                });
            }
        }, 1000);
    }, [delay]);

    return (
        <Container className={classes.Content}>
            <Container
                className={
                    status != GameStatus.READY ? classes.LoadingContent : classes.ContentDisable
                }
            >
                <CustomLoading text="Preparez-vous à jouer ..." />
            </Container>

            <Container
                className={
                    status == GameStatus.READY ? classes.ReadyContent : classes.ContentDisable
                }
            >
                <Title>La partie va démarrer dans :</Title>
                <p className={classes.ReadyCount}>{delay}</p>
            </Container>
        </Container>
    );
}
