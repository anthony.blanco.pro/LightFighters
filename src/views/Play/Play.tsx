import { ReactElement, useEffect, MouseEvent } from 'react';
import { useNavigate } from 'react-router-dom';
import { makeStyles } from '@mui/styles';
import { PlayStyles } from './Play.styles';

import PowerSettingsNewIcon from '@mui/icons-material/PowerSettingsNew';
import FormatListNumberedIcon from '@mui/icons-material/FormatListNumbered';
import Brightness3Icon from '@mui/icons-material/Brightness3';
import BuildIcon from '@mui/icons-material/Build';

import HardButton from '../../components/Buttons/HardButton/HardButton';
import IconsButton from '../../components/Buttons/IconsButton/IconsButton';
import { Grid } from '@mui/material';
import { getMetadata, updateGame } from '../../controllers/Game.controller';
import { GameStatus } from '../../enums/Game';
import { Metadata } from '../../interfaces/Game';

export default function Play(): ReactElement {
    const classes = makeStyles(PlayStyles)();
    const navigate = useNavigate();

    const wakingUp = (event: MouseEvent<HTMLElement>) => {
        getMetadata().then((metadata: Metadata) => {
            if (metadata.status === GameStatus.SLEEP) {
                updateGame('sleep').then((response) => {
                    if (response.status != 200) navigate('/error');
                });
            }
            navigate('/selector');
        });
    };

    const handleSleep = (event: MouseEvent<HTMLElement>) => {
        getMetadata().then((metadata: Metadata) => {
            updateGame('sleep').then((response) => {
                if (response.status != 200) navigate('/error');
                else {
                    if (metadata.status == undefined) navigate('/');
                }
            });
        });
    };
    return (
        <Grid container spacing={2}>
            <Grid item xs={12} className={classes.Play}>
                <HardButton
                    title="Jouer"
                    variant="contained"
                    class={PlayStyles.PlayButton}
                    onClick={wakingUp}
                />
            </Grid>
            <Grid container className={classes.Icons}>
                <Grid item xs={3} className={classes.PlayIcon}>
                    <IconsButton icon={BuildIcon} disable={true} />
                </Grid>
                <Grid item xs={3} className={classes.PlayIcon}>
                    <IconsButton
                        icon={FormatListNumberedIcon}
                        class={PlayStyles.PlayIcon}
                        disable={true}
                    />
                </Grid>
                <Grid item xs={3} className={classes.PlayIcon}>
                    <IconsButton
                        icon={Brightness3Icon}
                        class={PlayStyles.PlayIcon}
                        onClick={handleSleep}
                    />
                </Grid>
                <Grid item xs={3} className={classes.PlayIcon}>
                    <IconsButton
                        icon={PowerSettingsNewIcon}
                        onClick={() => {
                            navigate('/');
                        }}
                        class={PlayStyles.PlayIcon}
                    />
                </Grid>
            </Grid>
        </Grid>
    );
}
