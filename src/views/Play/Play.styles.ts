const PlayStyles: any = {
    Play: {
        textAlign: 'center !important',
        marginTop: '9.5rem !important',
        marginBottom: '1rem !important',
    },
    PlayButton: {
        fontSize: '28px !important',
    },
    Icons: {
        textAlign: 'center',
        marginTop: '4rem',
        marginLeft: '3rem',
        marginRight: '3rem',
    },
    PlayIcon: {
        marginLeft: '1rem',
        marginRight: '1rem',
        display: 'inline-block',
    },
};

export { PlayStyles };
