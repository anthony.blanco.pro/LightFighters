import { ReactElement, useEffect, useState } from 'react';
import { makeStyles } from '@mui/styles';
import { ErrorStyles } from './Error.styles';
import { Container } from '@mui/material';
import { useNavigate } from 'react-router-dom';
import CustomLoading from '../../components/CustomLoading/CustomLoading';

export function Error(): ReactElement {
    const classes = makeStyles(ErrorStyles)();
    const navigate = useNavigate();

    let [counter, setCounter] = useState(3);

    useEffect(() => {
        if (counter > 0) {
            setTimeout(() => {
                setCounter(counter - 1);
            }, 1000);
        } else {
            navigate('/');
        }
    }, [counter]);

    return (
        <Container className={classes.Text}>
            <p>{"Une erreur s'est produite"}</p>
            <CustomLoading text={'Vous allez être redirigé'} />
        </Container>
    );
}
