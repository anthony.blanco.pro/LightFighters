export const ErrorStyles: any = {
    Text: {
        marginTop: '15vh !important',
        fontFamily: 'Poppins-Bold !important',
        fontWeight: 'bold',
        fontSize: '26px !important',
        textAlign: 'center !important',
        color: '#FFFFFF !important',
        margin: '0 !important',
    },
};
