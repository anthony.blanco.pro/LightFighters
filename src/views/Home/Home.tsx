import { Grid } from '@mui/material';
import { ReactElement } from 'react';
import KeyboardControlKeyIcon from '@mui/icons-material/KeyboardControlKey';

import Icons from '../../components/Icons/Icons';
import Image from '../../components/Image/Image';
import { HomeStyles } from './Home.styles';
import { useNavigate } from 'react-router-dom';
import { makeStyles } from '@mui/styles';
import { getMetadata } from '../../controllers/Game.controller';
import { GameStatus } from '../../enums/Game';
import { Metadata } from '../../interfaces/Game';

export default function Home(): ReactElement {
    const classes = makeStyles(HomeStyles)();
    const navigate = useNavigate();

    return (
        <Grid
            container
            justifyContent="center"
            alignItems="center"
            className={classes.Home}
            onClick={() => {
                navigate('/play');
            }}
            onTouchMove={() => {
                navigate('/play');
            }}
        >
            <Grid item xs={12} className={classes.Logo}>
                <Image src="images/logo_lightfighters.png" alt="Logo LightFighters" />
            </Grid>
            <Grid container justifyContent="center" alignItems="center" className={classes.Footer}>
                <Grid item xs={3} className={classes.Icons}>
                    <Icons icon={KeyboardControlKeyIcon} class={classes.Icon} />
                    <Icons icon={KeyboardControlKeyIcon} class={classes.Icon} />
                    <Icons icon={KeyboardControlKeyIcon} class={classes.Icon} />
                    <Icons icon={KeyboardControlKeyIcon} class={classes.Icon} />
                    <Icons icon={KeyboardControlKeyIcon} class={classes.Icon} />
                    <Icons icon={KeyboardControlKeyIcon} class={classes.Icon} />
                </Grid>
                <Grid className={classes.Icons} item xs={4}>
                    <p> Déverrouiller </p>
                </Grid>
                <Grid item xs={3} className={classes.Icons}>
                    <Icons icon={KeyboardControlKeyIcon} class={classes.Icon} />
                    <Icons icon={KeyboardControlKeyIcon} class={classes.Icon} />
                    <Icons icon={KeyboardControlKeyIcon} class={classes.Icon} />
                    <Icons icon={KeyboardControlKeyIcon} class={classes.Icon} />
                    <Icons icon={KeyboardControlKeyIcon} class={classes.Icon} />
                    <Icons icon={KeyboardControlKeyIcon} class={classes.Icon} />
                </Grid>
            </Grid>
        </Grid>
    );
}
