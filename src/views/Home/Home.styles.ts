const HomeStyles: any = {
    Home: {
        width: '100% !important',
    },
    Logo: {
        marginTop: '8.5rem !important',
    },
    Footer: {
        marginTop: '3.7rem !important',
        fontFamily: 'Poppins-Bold !important',
        fontSize: '24px',
        fontWeight: 'bold !important',
        color: '#FFFFFF',
        textAlign: 'center',
    },
    Icon: {
        marginBottom: '-0.6rem !important',
    },
    Icons: {
        marginLeft: '1rem !important',
        marginRight: '1rem !important',
    },
};

export { HomeStyles };
