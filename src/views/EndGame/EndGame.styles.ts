const EndGameStyles: any = {
    Content: {
        padding: '0 !important',
    },
    Score: {
        width: 'auto !important',
        marginTop: '3.5rem',
        marginLeft: '1rem !important',
        marginRight: '1rem !important',
        padding: '0 !important',
        borderRadius: '20px !important',
        background: '#141493 !important',
        border: '1px solid rgba(255, 255, 255, 0.51) !important',
        boxSizing: 'border-box !important',
        boxShadow: '0px 0px 10px rgba(0, 0, 0, 0.2) !important',
    },
    ActionsP1: {
        marginTop: '12.75rem',
        padding: '0 !important',
        textAlign: 'center !important',
    },
    ActionsP2: {
        marginTop: '9.50rem',
        padding: '0 !important',
        textAlign: 'center !important',
    },
    ActionsP3: {
        marginTop: '6.25rem',
        padding: '0 !important',
        textAlign: 'center !important',
    },
    ActionsP4: {
        marginTop: '3rem',
        padding: '0 !important',
        textAlign: 'center !important',
    },
    Button: {
        marginLeft: '0.2rem',
        marginRight: '0.2rem',
        fontSize: '18px !important',
        padding: '0.75rem 2.75rem !important',
    },
};

export { EndGameStyles };
