import { ReactElement, useState, useEffect } from 'react';
import { useNavigate } from 'react-router-dom';
import { makeStyles } from '@mui/styles';
import { Container } from '@mui/material';
import { EndGameStyles } from './EndGame.styles';
import { getMetadata, updateGame } from '../../controllers/Game.controller';
import CustomTable from '../../components/CustomTable/CustomTable';
import HardButtons from '../../components/Buttons/HardButton/HardButton';
import { Metadata } from '../../interfaces/Game';
import { GameStatus } from '../../enums/Game';
import CustomLoading from '../../components/CustomLoading/CustomLoading';

export default function EndGame(): ReactElement {
    const classes = makeStyles(EndGameStyles)();
    const navigate = useNavigate();
    const [metadata, setMetadata] = useState({} as Metadata);
    const [loading, setLoading] = useState(true);

    useEffect(() => {
        getMetadata().then((metadata: Metadata) => {
            if (metadata.status == undefined) navigate('/error');
            else {
                if (metadata.status !== GameStatus.STOPPED) navigate('/error');
                setMetadata(metadata);
                setLoading(false);
            }
        });
    }, []);

    const handleExit = () => {
        updateGame('clean').then((response) => {
            if (response.status == 200) navigate('/play');
            else navigate('/error');
        });
        return;
    };

    const handleRestart = () => {
        updateGame('restart').then((response) => {
            if (response.status == 200) navigate('/loading');
            else navigate('/error');
        });
        return;
    };
    return (
        <Container className={classes.Content}>
            {loading ? (
                <Container
                    sx={
                        loading
                            ? { display: 'block', marginTop: '33vh !important' }
                            : { display: 'none' }
                    }
                >
                    <CustomLoading text="Chargement ...." />
                </Container>
            ) : (
                <Container className={classes.Content}>
                    <Container className={classes.Score}>
                        <CustomTable heads={['#', 'Joueurs', 'Points']} data={metadata.players} />
                    </Container>
                    <Container className={classes['ActionsP' + metadata.players.length.toString()]}>
                        <HardButtons
                            title="Rejouer"
                            onClick={handleRestart}
                            class={EndGameStyles.Button}
                        />
                        <HardButtons
                            title="S'enregistrer"
                            class={EndGameStyles.Button}
                            disable={true}
                        />
                        <HardButtons
                            title="Quitter"
                            onClick={handleExit}
                            class={EndGameStyles.Button}
                        />
                    </Container>
                </Container>
            )}
        </Container>
    );
}
