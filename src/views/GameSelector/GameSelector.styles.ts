const GameSelectorStyles: any = {
    Title: {
        marginTop: '1.2rem !important',
        padding: '0 !important',
    },
    Content: {
        padding: '0 !important',
    },

    TabPanel: {
        margin: 'auto',
        maxWidth: '70vw',
    },

    leftNavigation: {
        position: 'absolute',
        top: '45%',
        left: '1vw',
    },
    rightNavigation: {
        position: 'absolute',
        top: '45%',
        right: '1vw',
    },

    playNavigation: {
        position: 'absolute',
        top: '45%',
        right: '1vw',
    },

    TabSpan: {
        '& .MuiTabs-indicator': {
            display: 'flex',
            justifyContent: 'center',
            backgroundColor: 'transparent',
            height: '4px',
        },
        '& .MuiTabs-indicatorSpan': {
            width: '100%',
            backgroundColor: '#47BCFE',
        },
    },

    TabActive: {
        padding: '5px 10px !important',
        color: '#FFFFFF !important',
        fontFamily: `Poppins-Bold !important`,
        fontWeight: 'bold !important',
        fontSize: '20px !important',
        textTransform: 'none !important',
    },

    TabNotActive: {
        padding: '5px 10px !important',
        color: '#FFFFFF !important',
        fontFamily: `Poppins !important`,
        fontSize: '19px !important',
        textTransform: 'none !important',
    },

    ChoosingContent: {
        '&::-webkit-scrollbar': {
            display: 'none',
        },
        padding: '0 !important',
        overflowY: 'scroll',
        maxHeight: '66.5vh',
    },
};

export { GameSelectorStyles };
