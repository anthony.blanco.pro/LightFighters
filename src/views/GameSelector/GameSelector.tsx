import { ReactElement, useState, SyntheticEvent, MouseEvent, useEffect } from 'react';
import { useNavigate } from 'react-router-dom';
import { makeStyles } from '@mui/styles';
import { TabContext, TabPanel } from '@mui/lab';
import { Grid, Tab, Container } from '@mui/material';

import ArrowCircleLeftIcon from '@mui/icons-material/ArrowCircleLeft';
import ArrowCircleRightIcon from '@mui/icons-material/ArrowCircleRight';
import NotStartedIcon from '@mui/icons-material/NotStarted';

import { GameSelectorStyles } from './GameSelector.styles';
import Title from '../../components/Title/Title';
import IconsButton from '../../components/Buttons/IconsButton/IconsButton';
import { CustomToggleButtonGroup } from '../../components/Buttons/ToggleButton/CustomToggleButtonGroup';
import { StyledTabs } from '../../components/Buttons/StyledTabs/StyledTabs';
import TextImage from '../../components/Buttons/TextImage/TextImage';

import {
    getInformations,
    getModes,
    getNameModes,
    setGame,
} from '../../controllers/Game.controller';
import { GameMode } from '../../interfaces/Game';
import CustomLoading from '../../components/CustomLoading/CustomLoading';

export default function GameSelector(): ReactElement {
    const changeTab = (event: SyntheticEvent, tab: string) => {
        const nextTab = tabs.find((page) => page.key === tab) || tabs[0];
        setCurrentTab(nextTab.key);
        setCurrentTitle(nextTab.title);
    };

    const handleClickNavigation = (previousOrNext: number) => {
        const nextIndices =
            (tabs.findIndex((page) => page.key === currentTab) || 0) + previousOrNext;

        if (nextIndices === -1) {
            navigate('/play');
            return;
        } else if (nextIndices === tabs.length) {
            setGame(informationMode.id, selectedDifficulty, selectedPlayersCount).then(
                (response) => {
                    if (response.status === 201) navigate('/loading');
                    else navigate('/error');
                },
            );
            return;
        }

        const nextTab = tabs[nextIndices];
        setCurrentTab(nextTab.key);
        setCurrentTitle(nextTab.title);
    };

    const selectGameMode = (event: MouseEvent<HTMLElement>, nextGameMode: string) => {
        setSelectedGameMode(nextGameMode === null ? nameModes[0] : nextGameMode);
    };

    const selectDifficulty = (event: MouseEvent<HTMLElement>, nextDifficulty: string) => {
        setSelectedDifficulty(
            nextDifficulty === null ? informationMode.defaultDifficulty : nextDifficulty,
        );
    };

    const selectPlayersCount = (event: MouseEvent<HTMLElement>, nextNbPlayer: number) => {
        setSelectedNbPlayers(nextNbPlayer === null ? 4 : nextNbPlayer);
    };

    const classes = makeStyles(GameSelectorStyles)();
    const navigate = useNavigate();
    const [loading, setLoading] = useState(false);
    let [nameModes, setNameModes] = useState([] as string[]);
    const [informationMode, setInformationMode] = useState({} as GameMode);
    const [selectedGameMode, setSelectedGameMode] = useState('');
    const [selectedDifficulty, setSelectedDifficulty] = useState('');
    const [currentTab, setCurrentTab] = useState('default');
    const [selectedPlayersCount, setSelectedNbPlayers] = useState(4);
    const [currentTitle, setCurrentTitle] = useState('');

    useEffect(() => {
        setLoading(true);
        getModes().then((gameModes) => {
            if (gameModes.length === 0) navigate('/error');
            setNameModes(getNameModes(gameModes));
            setSelectedGameMode(nameModes[0]);
            setSelectedDifficulty(informationMode.defaultDifficulty);
            setInformationMode(getInformations(gameModes, selectedGameMode));
            setLoading(false);
            setCurrentTitle((tabs.find((tab) => tab.key === currentTab) || tabs[0]).title);
        });
    }, []);

    useEffect(() => {
        setSelectedGameMode(nameModes[0]);
        setSelectedDifficulty(informationMode.defaultDifficulty);
    }, [nameModes, informationMode]);

    const tabDefault = {
        key: 'default',
        title: 'Mode de jeu',
        content: (
            <CustomToggleButtonGroup
                onChange={selectGameMode}
                value={selectedGameMode}
                items={nameModes}
                disposition="column"
            />
        ),
    };

    const tabDescription = {
        key: 'description',
        title: 'Description',
        content: (
            <TextImage image={informationMode.imageUrl} description={informationMode.description} />
        ),
    };
    const tabDifficulty = {
        key: 'difficulty',
        title: 'Difficulté',
        content: (
            <CustomToggleButtonGroup
                onChange={selectDifficulty}
                value={selectedDifficulty}
                items={['Facile', 'Normal', 'Difficile']}
                disposition="column"
            />
        ),
    };
    const tabPlayers = {
        key: 'players',
        title: 'Joueurs',
        content: (
            <CustomToggleButtonGroup
                onChange={selectPlayersCount}
                value={selectedPlayersCount}
                items={[1, 2, 3, 4]}
                disposition="row"
            />
        ),
    };
    const tabSummary = {
        key: 'summary',
        title: 'Résumé',
        content: (
            <CustomToggleButtonGroup
                onChange={changeTab}
                itemLinks={[tabDefault.key, tabPlayers.key, tabDifficulty.key]}
                items={[selectedGameMode, 'Joueurs : ' + selectedPlayersCount, selectedDifficulty]}
            />
        ),
    };
    const tabs = [tabDefault, tabDescription, tabPlayers, tabDifficulty, tabSummary];

    return (
        <Container className={classes.Content}>
            {loading ? (
                <Container
                    sx={
                        loading
                            ? { display: 'block', marginTop: '33vh !important' }
                            : { display: 'none' }
                    }
                >
                    <CustomLoading text="Chargement ...." />
                </Container>
            ) : (
                <Grid container sx={loading ? { display: 'none' } : { display: 'block' }}>
                    <Grid item xs={12} className={classes.Title}>
                        <Title>{currentTitle}</Title>
                    </Grid>
                    <Grid container className={classes.Content}>
                        <IconsButton
                            icon={ArrowCircleLeftIcon}
                            id="leftNavigation"
                            class={GameSelectorStyles.leftNavigation}
                            onClick={() => {
                                handleClickNavigation(-1);
                            }}
                        />
                        <IconsButton
                            icon={
                                currentTab === tabSummary.key
                                    ? NotStartedIcon
                                    : ArrowCircleRightIcon
                            }
                            id="rightNavigation"
                            class={
                                currentTab === tabSummary.key
                                    ? GameSelectorStyles.playNavigation
                                    : GameSelectorStyles.rightNavigation
                            }
                            onClick={() => {
                                handleClickNavigation(1);
                            }}
                        />
                        <TabContext value={currentTab}>
                            <StyledTabs
                                onChange={changeTab}
                                aria-label="My tunnel"
                                value={currentTab}
                            >
                                {tabs.map((tab, index) => {
                                    return (
                                        <Tab
                                            key={index}
                                            label={tab.title}
                                            value={tab.key}
                                            className={
                                                tab.key === currentTab
                                                    ? classes.TabActive
                                                    : classes.TabNotActive
                                            }
                                        />
                                    );
                                })}
                            </StyledTabs>
                            {tabs.map((tab, index) => {
                                return (
                                    <TabPanel
                                        key={index}
                                        value={tab.key}
                                        className={classes.TabPanel}
                                    >
                                        <Container className={classes.ChoosingContent}>
                                            {tab.content}
                                        </Container>
                                    </TabPanel>
                                );
                            })}
                        </TabContext>
                    </Grid>
                </Grid>
            )}
        </Container>
    );
}
