import { ReactElement, useEffect, useState } from 'react';
import { useNavigate } from 'react-router-dom';
import { makeStyles } from '@mui/styles';
import PauseCircleIcon from '@mui/icons-material/PauseCircle';
import { Container } from '@mui/material';

import { InGameStyles } from './InGame.styles';
import { getMetadata, updateGame } from '../../controllers/Game.controller';
import IconsButton from '../../components/Buttons/IconsButton/IconsButton';
import CustomTable from '../../components/CustomTable/CustomTable';
import CustomDialog from '../../components/CustomDialog/CustomDialog';
import { GameStatus } from '../../enums/Game';
import { Metadata, Time } from '../../interfaces/Game';
import CustomLoading from '../../components/CustomLoading/CustomLoading';

export default function InGame(): ReactElement {
    const classes = makeStyles(InGameStyles)();
    const navigate = useNavigate();
    const [metadata, setMetadata] = useState({} as Metadata);
    const [loading, setLoading] = useState(true);
    const [dialogState, setDialogState] = useState(false);
    const [time, setTime] = useState({} as Time);

    useEffect(() => {
        getMetadata().then((metadata: Metadata) => {
            if (metadata.status == undefined) navigate('/error');
            else if (metadata.status === GameStatus.NOT_READY) navigate('/');
            else if (metadata.status === GameStatus.LOADING || metadata.status === GameStatus.READY)
                navigate('/loading');
            else if (metadata.status === GameStatus.STOPPED) navigate('/endgame');
            else if (metadata.status === GameStatus.IN_PROGRESS || loading) {
                setMetadata(metadata);

                const formatTime = metadata.time / 1000;
                const minutes = ~~(formatTime / 60);
                const seconds = ~~formatTime % 60;
                setTime({
                    minutes:
                        minutes > 10 ? minutes.toString() : minutes.toString().padStart(2, '0'),
                    seconds:
                        seconds > 10 ? seconds.toString() : seconds.toString().padStart(2, '0'),
                });

                setLoading(false);
            } else if (metadata.status === GameStatus.PAUSED && !dialogState) {
                setDialogState(true);
            }
        });
    }, [metadata.time, dialogState, loading]);

    const handleOpenDialog = () => {
        updateGame('pause').then((response) => {
            if (response.status != 200) navigate('/error');
        });
    };

    const handleResume = () => {
        updateGame('resume').then((response) => {
            if (response.status != 200) navigate('/error');
            else setDialogState(false);
        });
    };

    const handleRestart = () => {
        updateGame('restart').then((response) => {
            if (response.status != 200) navigate('/error');
            else setDialogState(false);
        });
    };

    const handleExit = () => {
        updateGame('stop').then((response) => {
            if (response.status != 200) navigate('/error');
            else setDialogState(false);
        });
    };

    return (
        <Container className={classes.Content}>
            {loading ? (
                <Container
                    sx={
                        loading
                            ? { display: 'block', marginTop: '33vh !important' }
                            : { display: 'none' }
                    }
                >
                    <CustomLoading text="Chargement ...." />
                </Container>
            ) : (
                <Container className={classes.Content}>
                    <Container className={classes.Time}>
                        {time.minutes} : {time.seconds}
                    </Container>
                    <Container className={classes.Score}>
                        <CustomTable heads={['#', 'Joueurs', 'Points']} data={metadata.players} />
                    </Container>
                    <Container className={classes['PauseP' + metadata.players.length.toString()]}>
                        <IconsButton
                            icon={PauseCircleIcon}
                            class={InGameStyles.pause}
                            onClick={handleOpenDialog}
                        />
                    </Container>
                    <CustomDialog
                        dialogState={dialogState}
                        handleResume={handleResume}
                        handleRestart={handleRestart}
                        handleExit={handleExit}
                    />
                </Container>
            )}
        </Container>
    );
}
