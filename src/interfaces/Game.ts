import { Player } from './Player';

export interface GameMode {
    id: number;
    name: string;
    author: string;
    description: string;
    type: number;
    minPlayer: number;
    maxPlayer: number;
    defaultDifficulty: string;
    duration: number;
    imageUrl?: string | undefined;
    maxScore: number;
    groupAllowed: boolean;
}

export interface Metadata {
    time: number;
    type: number;
    status: number;
    players: Player[];
}

export interface Time {
    minutes: string;
    seconds: string;
}
