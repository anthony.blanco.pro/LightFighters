const AppLayoutStyles = {
    AppLayout: {
        display: 'flex',
        margin: '0',
        width: '100%',
        height: '100vh',
        background: 'radial-gradient(88.65% 191.35% at 50% 50%, #141493 0%, #000234 100%)',
        PointerEvent: 'none ',
        cursor: 'none',
    },
};

export { AppLayoutStyles };
