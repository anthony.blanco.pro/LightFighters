import { ReactElement } from 'react';
import { AppLayoutStyles } from './AppLayout.styles';
import Container from '@mui/material/Container';
import { makeStyles } from '@mui/styles';

export default function AppLayout({ children }: any): ReactElement {
    const classes = makeStyles(AppLayoutStyles)();

    return (
        <div className={classes.AppLayout}>
            <Container maxWidth="lg">{children}</Container>
        </div>
    );
}
