import { ReactElement } from 'react';
import { BrowserRouter, Route, Routes } from 'react-router-dom';

import Loading from './views/Loading/Loading';
import GameSelector from './views/GameSelector/GameSelector';
import Home from './views/Home/Home';
import Play from './views/Play/Play';
import InGame from './views/InGame/InGame';
import EndGame from './views/EndGame/EndGame';
import { Error } from './views/Error/Error';

export default function Router(): ReactElement {
    return (
        <BrowserRouter>
            <Routes>
                <Route path="/" element={<Home />} />
                <Route path="/play" element={<Play />} />
                <Route path="/selector" element={<GameSelector />} />
                <Route path="/loading" element={<Loading />} />
                <Route path="/ingame" element={<InGame />} />
                <Route path="/endgame" element={<EndGame />} />
                <Route path="/error" element={<Error />} />
            </Routes>
        </BrowserRouter>
    );
}
