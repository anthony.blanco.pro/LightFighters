// OLED screen settings
#define SCREEN_WIDTH 128
#define SCREEN_HEIGHT 64
#define SCREEN_ADDRESS 0x3C

// Serial protocol
#define SERIAL_BEGIN_CHAR '!'
#define SERIAL_END_CHAR '"'
#define SERIAL_PADDING_CHAR '*'

// Sleep delay in seconds
#define DELAY_SLEEP 30

// Mux address
#define MUX_ADDR 0x70

// Version code address in EEPROM
#define VERSION_CODE_ADDR 0

// Error code value
#define ERROR_CODE "4845492D456E63756CE973"
