#include <SPI.h>
#include <Wire.h>
#include <EEPROM.h>
#include <Adafruit_GFX.h>
#include <Adafruit_SSD1306.h>
#include "Settings.h"
#include "Bitmaps.h"


// OLEDs Init
Adafruit_SSD1306 display(SCREEN_WIDTH, SCREEN_HEIGHT, &Wire, -1);


// Global vars
char versionCode[5]; // Version code
char messages[4][20]; // Data from incoming command
int mode = -1; // Mode from the received command
bool startup = true; // True if we've just booted and not received any command
long prevMillis; // Timer for non blocking delays


void setup() {

  Serial.begin(9600); // Init Serial

  Wire.begin(); // Init I2c

  EEPROM.get(VERSION_CODE_ADDR, versionCode); // Retreive versionCode from EEPROM

  // Init all 4 OLEDs
  for (int displayNumber = 0; displayNumber < 4; displayNumber++) {

    tcaSelect(displayNumber); // Select OLED

    // Halt exec if an oled is missing
    if (!display.begin(SSD1306_SWITCHCAPVCC, SCREEN_ADDRESS)) {

      Serial.print(F("OLED"));
      Serial.print(displayNumber);
      Serial.println(F(" NOT FOUND"));
      while (true); // Stop exec

    }
  }

  // Show splash for 1 sec on all 4 OLEDs
  viewSplash();
  displayOnAll();
  delay(1000);

}


void loop() {

  char buf[27] = {0}; // Create 27 character buffer

  // Check if any data has been received on serial port
  if (Serial.available() > 0) {

    int len = Serial.readBytes(buf, 27); // Retreive the data

    // Check if data is properly formatted (begin and end char)
    if (buf[0] == SERIAL_BEGIN_CHAR && buf[26] == SERIAL_END_CHAR) {

      startup = false; // We've now received a command, we are no longer on boot
      prevMillis = millis(); // Update time for non blocking delays
      mode = buf[1] - 48; // Retreive mode as ascii and store as int for better readability

      // Fill messages with the received data minus the mode char, the begin and the end char
      for (int k = 0; k < 4; k++) {

        for (int i = 0; i < 6; i++) {

          messages[k][i] = buf[(k * 6) + (i + 2)];

        }

      }

    }

    Serial.flush(); // Empty the serial buffer

  }

  // Trigger "SCREENSAVER" if no commands bave been received in the set delay
  if ((prevMillis + 1000 * DELAY_SLEEP <= millis()) && !startup)
    mode = 4;

  // Get into the specified mode
  // For some 'Arduino' reason switch case does not work properly here
  if (mode == 0) {
    viewScore();
  }
  else if (mode == 1) {
    viewLives();
  }
  else if (mode == 2) {
    while (Serial.available() == 0) {
      viewLoader();
    }
  }
  else if (mode == 3) {
    viewSplash();
    displayOnAll();
  }
  else if (mode == 4) {
    viewSleep();
  }
  else if (mode == 5) {
    viewTime();
  }
  else if (mode == 6) {
    viewRank();
  }
  else if (mode == 7) {
    viewPersRank();
  }
  else if (mode == 8) {
    viewCredits();
    displayOnAll();
  }
  else if (mode == 9) {
    updateVersionCode();
    mode = -1;
  }
  else if (mode == 10) {
    viewError();
    displayOnAll();
  }
  else if (mode == 11) {
    viewPause();
    displayOnAll();
  }
  else {
    viewLoader();
  }

}


// Call this to update the version code in EEPROM
void updateVersionCode() {

  // Store the received version code into the proper var
  for (int k = 0; k < 4; k++)
    versionCode[k] = messages[0][k];

  versionCode[4] = '\0'; // Add end of string char

  EEPROM.put(VERSION_CODE_ADDR, versionCode); // Store the new version code in EEPROM

}


// Call this to select which screen to write data to
void tcaSelect (uint8_t i) {

  if (i > 3) return; // Exit if the screen does not exist

  Wire.beginTransmission(MUX_ADDR); // Establish connection to the mux address
  Wire.write(1 << i); // Select the mux port
  Wire.endTransmission(); // Kill connection to the mux to free the bus

}


// Call this to get X position for centering an item on the OLED
int getPosX(int textLen, int charWidth) {

  return (SCREEN_WIDTH - (textLen * charWidth)) / 2;

}


// Call this to draw on all 4 OLEDs
void displayOnAll(void) {

  for (int displayNumber = 0; displayNumber < 4; displayNumber++) {
    tcaSelect(displayNumber); // Select the screen
    display.display(); // Draw items on screen
  }

}


// Call this to set the font size and color
void setFont(int fontSize) {

  display.setTextSize(fontSize); // Configure font size
  display.setTextColor(SSD1306_WHITE); // Configure font "color"

}


// Call this to show the score view for each player
void viewScore(void) {

  viewGeneric((char*)"SCORE");

}


// Call this to show the time view for each player
void viewTime() {

  viewGeneric((char*)"TIME");

}


// Call this to show the loading screen on all 4 OLEDs
void viewLoader() {

  for (int k = -96; k <= 128; k += 32) {

    viewSplash(); // Show splash screen

    display.fillRect(k, 60, 96, 2, SSD1306_WHITE); // Add the scrolling bar

    displayOnAll(); // Draw items on screen
  }

}


// Call this to get the actual message length by removing padding
int getTextLength(int pos) {

  int nbChar = 6; // Max number of chars by message

  for (int k = 0; k < nbChar; k++) {

    // Exit when a padding char gets encountered
    if (messages[pos][k] == SERIAL_PADDING_CHAR)
      nbChar = k;

  }

  return nbChar;

}


// Call this to display the game ranking view
void viewRank() {

  drawTitle((char*)"RANKING"); // Draw the title

  int nbChar = getTextLength(0); // Get number of chars to display for the winner

  setFont(2); // Set font color and size for the winner

  int offsetX = getPosX(nbChar + 3, 12); // Calculate X offset for centering the winner's name

  display.setCursor(offsetX, 16); // Set cursor position

  // Fill screen buffer
  for (int rank = 0; rank < 4; rank++) {

    // Put player name into buffer without the padding
    nbChar = getTextLength(rank);

    // Create view only for players and show nothing
    if (nbChar > 0) {

      // Put rank into buffer
      display.print("#");
      display.print(rank + 1);
      display.print(" ");

      for (int k = 0; k < nbChar; k++)
        display.print(messages[rank][k]);

    }

    display.setCursor(offsetX, 23 + (1 + rank) * 10); // Set cursor position to align with the winner's name
    setFont(1); // Set font color and size for other players

  }

  displayOnAll(); // Draw on all displays

}


// Call this to draw the title in the yellow bar
void drawTitle(char* title) {

  display.clearDisplay(); // Rid the screen buffer from all shit

  setFont(1); // Set font color and size

  // Prepare the screen buffer
  display.setCursor(getPosX(strlen(title), 6), 0);
  display.println(title);

}


// Call this to display a player's personnal rank
void viewPersRank() {

  for (int displayNumber = 0; displayNumber < 4; displayNumber++) {

    tcaSelect(displayNumber); // Select current display

    int rank = messages[displayNumber][0];

    // Create view only for players and show splash for others
    if (rank == SERIAL_PADDING_CHAR) {

      viewSplash(); // Display splash screen

    }
    else {

      drawTitle((char*)"RANK"); // Draw the title

      setFont(5); // Set font color and size

      // Prepare the screen buffer
      display.setCursor(getPosX(2, 30), 20);
      display.print("#");
      display.print(rank  - 48);

    }

    // Call the drawer for active screen
    display.display();

  }

}


// Call this to display a generic view by only providing a title
void viewGeneric(char* nom) {

  for (int displayNumber = 0; displayNumber < 4; displayNumber++) {

    tcaSelect(displayNumber); // Select current display

    int nbChar = getTextLength(displayNumber); // Get number of chars to draw

    // Create view only for players and show splash for others
    if (nbChar == 0) {

      viewSplash(); // Display splash screen

    }
    else {

      drawTitle(nom); // Draw the title

      setFont(3); // Set font color and size

      // Prepare the screen buffer
      display.setCursor(getPosX(nbChar, 18), 25);
      for (int k = 0; k < nbChar; k++) {

        display.print(messages[displayNumber][k]);

      }

    }

    // Call the drawer for active screen
    display.display();

  }

}


// Call this to show the lives view for each player
// This implementation is terrible but I do not have the time to improve it
void viewLives(void) {

  for (int displayNumber = 0; displayNumber < 4; displayNumber++) {

    tcaSelect(displayNumber); // Select active display

    // Parse number of lives and number of remaining lives (we remove 48 for ASCII)
    int nbLives = messages[displayNumber][0] - 48;
    int nbLivesRem = messages[displayNumber][1] - 48;

    // Create view only for players and show splash for others
    if (nbLives == SERIAL_PADDING_CHAR - 48) {

      viewSplash(); // Display splash screen

    }
    else {

      drawTitle((char*)"VIES"); // Draw the title

      // If player has more lives remaining than lives then exit
      if (nbLivesRem > nbLives)
        return;

      // Do we need to draw hearts, check if the player is still alive
      if (nbLivesRem == 0) {

        setFont(5); // Set font color and size

        // Put message into the screen buffer with proper formatting
        display.setCursor(getPosX(3, 30), 20);
        display.println(F("K.O"));

      }
      else {

        // Case where length of nbLives and nbLivesRem = 10 the sent char is A as in HEX A=10
        if (messages[displayNumber][0] == 65)
          nbLives = 10;
        if (messages[displayNumber][1] == 65)
          nbLivesRem = 10;

        int posY = 0; // Y pos
        int posX = 0; //X pos

        posY = 18; // Default Y pos for line 1

        // How many hearts on line 1 and line 2
        int nbHeartA = nbLives; // Number of hearts on line 1
        int nbHeartB = 0; // Number of hearts on line 2

        if (nbLives > 5) {

          nbHeartA = 5;
          nbHeartB = nbLives - 5;

        }

        // Place alive hearts
        for (int nbHeartToDraw = 0; nbHeartToDraw < nbLivesRem; nbHeartToDraw++) {

          posX = 20 * nbHeartToDraw + getPosX(nbHeartA, 20);

          if (nbHeartToDraw >= 5) {

            posY = 40;
            posX = 20 * (nbHeartToDraw - 5) + getPosX(nbHeartB, 20);

          }

          display.drawBitmap(posX, posY, HEART_ALIVE_BMP, 16, 16, 1);

        }

        // Reset Y and X default pos
        posY = 18;
        posX = 0;

        // Place dead hearts
        for (int nbHeartToDraw = nbLivesRem; nbHeartToDraw < nbLives; nbHeartToDraw++) {

          posX = 20 * nbHeartToDraw + getPosX(nbHeartA, 20);

          if (nbHeartToDraw >= 5) {

            posY = 40;
            posX = 20 * (nbHeartToDraw - 5) + getPosX(nbHeartB, 20);

          }

          display.drawBitmap(posX, posY, HEART_DEAD_BMP, 16, 16, 1);

        }

      }

    }

    // Call the drawer for the current screen
    display.display();

  }

}


// Call this to show the "SCREENSAVER" on all OLEDs
void viewSleep(void) {


  if (millis() <= prevMillis + 5000) {

    // Show launch game prompt
    viewLaunchGame();
    displayOnAll();

  }
  else if (millis() >= prevMillis + 6500) {

    prevMillis = millis(); // Update prevMillis

  }
  else {

    // Show splash screen
    viewSplash();
    displayOnAll();

  }

}


// Call this to put the credits into the screen buffer
void viewCredits(void) {

  drawTitle((char*)"LIGHTFIGHTERS"); // Put title into the screen buffer

  // Put credits into the screen buffer with proper formatting
  display.setCursor(getPosX(7, 6), 8);
  display.println(F("credits"));
  display.setCursor(getPosX(11, 6), 16);
  display.println(F("Louis NAILI"));
  display.setCursor(getPosX(13, 6), 24);
  display.println(F("Anthony BLANCO"));
  display.setCursor(getPosX(13, 6), 32);
  display.println(F("Julien WAGNER"));
  display.setCursor(getPosX(18, 6), 40);
  display.println(F("Alexandre GROSSARD"));
  display.setCursor(getPosX(15, 6), 48);
  display.println(F("Raphael VERBEKE"));
  display.setCursor(getPosX(14, 6), 56);
  display.println(F("Tristan FAVREL"));

}


// Call this to show the "FATAL ERROR" message
void viewError(void) {

  drawTitle((char*)"FATAL ERROR"); // Put title into the screen buffer

  // Put error into the screen buffer with proper formatting
  display.setCursor(getPosX(6, 6), 25);
  for (int k = 0; k < 22; k++) {

    if (k == 6)
      display.setCursor(getPosX(2, 6), 33);

    if (k == 8)
      display.setCursor(getPosX(14, 6), 42);

    display.print(ERROR_CODE[k]);

  }

}


// Call this to put the "LAUNCH GAME" message into the screen buffer
void viewLaunchGame(void) {

  display.clearDisplay(); // Rid the screen buffer from all shit

  setFont(1); // Set font color and size

  // Put message into the screen buffer with proper formatting
  display.setCursor(getPosX(18, 6), 20);
  display.println(F("Pour lancer le jeu"));
  display.setCursor(getPosX(20, 6), 30);
  display.println(F("selectionnez un mode"));
  display.setCursor(getPosX(21, 6), 40);
  display.println(F("sur l'ecran principal"));

}


// Call this to put the splash into the screen buffer
void viewSplash(void) {

  display.clearDisplay(); // Rid the screen buffer from all shit

  setFont(1); // Set font color and size

  // Set parameters and put splash screen into the screen buffer
  display.drawBitmap(0, 16, SPLASH_BMP, 128, 32, 1);

  // Only show version on startup
  if (startup) {

    // Put version code into the screen buffer
    display.setCursor(95, 50);
    display.print(F("V"));
    display.println(versionCode);

  }

}


// Call this to put the pause bitmap into the screen buffer
void viewPause(void) {

  display.clearDisplay(); // Rid the screen buffer from all shit

  setFont(1); // Set font color and size

  // Set parameters and put splash screen into the screen buffer
  display.drawBitmap(48, 16, PAUSE_BMP, 32, 32, 1);

}
