#!/usr/bin/env bash

# Variables
projectName='lightfighters'
projectPath='/home/pi/Projects'
finalPath="$projectPath/$projectName"
servicePath="/etc/systemd/system/$projectName.service"
compressedProject="$projectName.tar.gz"
availableEnv="local remote staging production"

remoteIp=
env="local"
version="latest"
module=0

while getopts "v:e:a:m" OPT; do
    case $OPT in
    v)
        version="$OPTARG"
        ;;
    e)
        env="$OPTARG"
        ;;
    a)
        env="remote"
        remoteIp="$OPTARG"
        ;;
    m)
        module=1
        ;;
    *)
        exit 1
        ;;
    esac
done

isEnvValid=0
for currEnv in $availableEnv; do
    if [ "$currEnv" == "$env" ]; then
        isEnvValid=1
    fi
done

if [ $isEnvValid -eq 0 ]; then
    echo "[ERROR] Invalid environment : '$env'"
    exit 2
fi

deployedName="$projectName-$version"

### Write message and return exit code
function exitIfError() {
    if [ "$1" != 0 ]; then
        echo -e "----> ERROR [Return Code = $1]: $2"
        exit "$1"
    fi
}

echo -e "========================================================="
echo -e "========== Starting Deployment of Lightfighters =========\n"

### If version is currently in production and running, stop game service, re-create symbolic link and restart game service

# Check if deployment must be done remotely
if [ "$env" == "remote" ]; then

    echo "----> Deploying using local network at $remoteIp"
    result=""
    files="build"
    if [ "$module" == "1" ]; then
        echo -e "\nAre you sure you want to transfer node modules (~5-10min) ? [N/y]"
        read -r result
    fi
    if [[ $result =~ ^[yY]([eE][sS])?$ ]]; then
        echo -e "\t'--> Include modules in deployment : true"
        files="$files node_modules"
    else
        echo -e "\t'--> Include modules in deployment : false"
    fi

    if [ -f "$compressedProject" ]; then
        echo "----> Removing old compressed files"
        rm "$compressedProject"
    fi

    echo "----> Compressing files : $files in $compressedProject"
    tar -cvzf $compressedProject "$files" > /dev/null
    exitIfError $? "ERROR: Unable to compress files. Exiting..."

    deployPath="$remoteIp:/tmp/"
    echo "----> Transferring compressed files to $deployPath"
    scp -i "$HOME/.ssh/staging_key.pub" "$compressedProject" "$remoteIp:/tmp/"

    CMD="[ -d \"$finalPath/build\" ] && sudo rm -r \"$finalPath/build\"; sudo tar -xvzf \"/tmp/$compressedProject\" -C \"$finalPath\" > /dev/null"
    echo -e "----> Executing deploy command on '$remoteIp' : \n$CMD"
    ssh -i "$HOME/.ssh/staging_key.pub" "$remoteIp" "$CMD"

else

    # Stop lightfighters service if exists
    if [ -f "$servicePath" ]; then
        echo "----> Stopping game"
        systemctl stop lightfighters
    else
        echo "----> Game not started. Service not found"
    fi

    # Removing symbolic link if exist
    if [ -e "$finalPath" ]; then
        echo "----> Removing current game version"
        INSTALLED_VERSION=$(readlink -f $finalPath | cut -d '/' -f3)
        [[ $INSTALLED_VERSION =~ $projectName-* ]] && echo "$INSTALLED_VERSION" > "$finalPath/.oldVersion" || echo "" > "$finalPath/.oldVersion"
        rm "$finalPath"
    fi

    if [ "$env" != "local" ]; then
        # Changing file owner and group
        chown -R pi:pi "$projectPath/$deployedName"
    fi

    # Creating symbolic link
    echo "----> Installing new lightfighters version"
    if [ "$env" == "local" ]; then
        deployedName=$(pwd)
    fi

    cd "$projectPath"
    ln -s "$deployedName" "$projectName"
    exitIfError $? "Unable to install Lightfighters"

    if [ ! -f "$servicePath" ]; then
        # Create lightfighters game service
        echo "----> Creating game service"
        {
            echo "[Unit]"
            echo "Description=Lightfighters Game"
            echo "After=network.target"
            echo "ConditionPathExists=$projectPath/$deployedName"
            echo ""
            echo "[Service]"
            echo "Type=simple"
            echo "PIDFile=/var/run/lightfighters/game.pid"
            echo "ExecStart=/usr/bin/npm start --prefix $projectPath/$deployedName"
            echo "ExecStop=/usr/bin/pkill chrome && /usr/bin/pkill chromium"
            echo "Restart=on-failure"
            echo "TimeoutStartSec=5"
            echo "TimeoutStopSec=10"
            echo "User=root"
            echo "Group=root"
            echo ""
            echo "[Install]"
            echo "WantedBy=multi-user.target"
            echo ""
        } >> "$servicePath"
        exitIfError $? "Unable to create service configuration file"
    fi

    echo "$deployedName" > $projectPath/.currentVersion

    # Start lightfighters service if exists
    echo "----> Starting game service"
    systemctl start lightfighters
    exitIfError $? "Unable to start Lightfighters game. You should perform rollback action"

fi

echo -e "\n=========== Ending Deployment of Lightfighters =========="
echo -e "========================================================="
