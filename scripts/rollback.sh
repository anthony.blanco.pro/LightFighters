#!/usr/bin/env bash

# Variables
projectName='lightfighters'
projectPath='/project'
productionPath="$projectPath/$projectName"
serviceName=lightfighters.service

OLD_VERSION=$(cat $projectPath/.oldVersion)
CURR_VERSION=$(cat $projectPath/.currentVersion)

### Write message and return exit code
function exitIfError() {
    if [ "$1" != 0 ]; then
        echo -e "----> ERROR [Return Code = $1]: $2"
        exit "$1"
    fi
}

echo -e "\n========================================================="
echo -e "\n========== Starting Rollback of Lightfighters ==========="

if [ ! -d "$productionPath" ]; then
    exitIfError 1 "Lightfighter game is not installed or not found"
fi

if [ -d "$projectPath/$OLD_VERSION" ]; then
    exitIfError 1 "Old version not found"
fi

# Stopping lightfighters game
echo "----> Stopping installed version"
systemctl stop lightfighters

# Removing current game version
echo "----> Removing installed game"
rm -r "$productionPath"

# Re-installing old version if exists
oldVersion="$(cat "$projectPath/$OLD_VERSION")"
if [ "$oldVersion" != "" ]; then
    echo "----> Re-installing old version ($oldVersion)"
    ln -s "$projectPath/$OLD_VERSION" "$productionPath"

    # Re-starting lightfighters game
    echo "----> Restarting rollbacked version"
    systemctl start lightfighters

    echo "" > "$projectPath/.oldVersion"
    echo "$oldVersion" > "$projectPath/.currentVersion"
else
    echo "----> No old version found"
fi

echo -e "\n============ Ending Rollback of Lightfighters ==========="
echo -e "\n========================================================="
