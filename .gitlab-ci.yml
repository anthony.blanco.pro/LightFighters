#
# Gitlab CI/CI for Lightfighters project.
#

stages:
    - linter
    - deploy

image: node:14.15.0

# Default variables
variables:
    APP_VERSION: 'latest'
    APP_NAME: 'lightfighters'
    APP_NAME_VERSIONED: '${APP_NAME}-${APP_VERSION}'
    APP_MODULES_DIR: 'node_modules'
    DEPLOY_PATH: '/project/${APP_NAME_VERSIONED}'
    FORCE_PROD: 'false'
    SKIP_DEPLOY: 'false'

# ------------------------------------- #
# ------------ Linter Stage ----------- #
# ------------------------------------- #

# Lint job running prettier framework
lint:
    stage: linter
    script:
        - npm install --production
        - npm run format
        - npm run lint
    cache:
        paths:
            - ${APP_MODULES_DIR}

# ------------------------------------- #
# ---------- Deploy Stage ------------- #
# ------------------------------------- #

.deploy:
    script:
        - npm install --production
        - npm run build
        - npm run package
        - \[ -d "${DEPLOY_PATH}" \] && rm -r "${DEPLOY_PATH}"
        - mkdir ${DEPLOY_PATH}
        - tar -xvzf ${APP_NAME}.tar.gz -C ${DEPLOY_PATH}
        - echo "Deployed '${APP_NAME_VERSIONED}' in ${DEPLOY_PATH}."
    rules:
        - if: '$SKIP_DEPLOY == "true"'
          when: never
        - if: $CI_MERGE_REQUEST_ID
          when: never

.rules_stg:
    rules:
        - !reference [.deploy, rules]
        - when: manual
    script:
        - bash $DEPLOY_PATH/scripts/deploy.sh -e staging -v $APP_NAME_VERSIONED

.rules_prod:
    rules:
        - !reference [.deploy, rules]
        - if: '$FORCE_PROD == "true" || $CI_COMMIT_BRANCH =~ /^(master|dev)$/'
          when: manual
        - when: never
    script:
        - bash $DEPLOY_PATH/scripts/deploy.sh -e production -v $APP_NAME_VERSIONED
    needs:
        - job: lint

# Deploy in staging (Pre-Prod). Only available if 'FORCE_PROD' variable is set to 'false' (default: false)
deploy-staging:
    stage: deploy
    rules:
        - !reference [.rules_stg, rules]
    script:
        - !reference [.deploy, script]
        - !reference [.rules_stg, script]
    tags:
        - rpi-dev

# Deploy in production. Only available if 'FORCE_PROD' variable is set to 'true' (default: false) or current branch name is master or dev
deploy-production:
    stage: deploy
    rules:
        - !reference [.rules_prod, rules]
    script:
        - !reference [.deploy, script]
        - !reference [.rules_prod, script]
    needs:
        - !reference [.rules_prod, needs]
    tags:
        - rpi-prod
