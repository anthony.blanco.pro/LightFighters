# LightFigthers v2

Lightfighters v2 is a multiplayer game using LEDs created at ISEN by AP5-63 school year. This game is available

<hr>

## Summary

-   [Installation](https://gitlab.com/isen-projects/LightFighters#installation)
-   [Troubleshooting](https://gitlab.com/isen-projects/LightFighters#troubleshooting)

<hr>

## Installation

<span style="color: red"><b>The project is build for running on Raspberry Pi. You'll not be able to install it on any other device.</b></span>

### Step 0 : Hardware connection

-   Addressable LED (WS291X Strip) on <i><b>GPIO 18</b></i>

### Step 1 : Clone the repository

```bash
git clone https://gitlab.com/isen-projects/LightFighters.git
```

### Step 2 : Change directory and install project dependencies

```bash
cd ./LightFighters && npm install
```

### Step 3 : Start the game. The game will be launch in background. Use `npm run stop` to stop it.

```bash
sudo npm run start
```

<hr>

## Troubleshooting

### WS281X Module generate random color

Disable audio module by commenting it in /boot/config.txt and add theses lines as bellow :

```txt
# Enable audio (loads snd_bcm2835)
# dtparam=audio=on

hdmi_force_hotplug=1
hdmi_force_edit_audio=1
```

PWM module is also used by audio module of Raspberry and cause multiple issues.

### Disable bluetooth module for serial

Bluetooth module is using the serial interface of the pi and oled screens are using the same interface. To prevent conflics, disable this module by following theses instructions :

In /boot/config.txt, add these lines at the end :

```txt
dtoverlay=pi3-disable-bt
```

### Confi

### Enable 